"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.endpointsSchema = void 0;
const joi_1 = __importDefault(require("joi"));
const component = joi_1.default.alternatives().try(joi_1.default.object().unknown(), joi_1.default.func());
exports.endpointsSchema = joi_1.default.array().items(joi_1.default.object({
    path: joi_1.default.string(),
    method: joi_1.default
        .string()
        .valid("get", "head", "post", "put", "patch", "delete", "connect", "options"),
    root: joi_1.default.bool(),
    handler: joi_1.default.alternatives().try(joi_1.default.array().items(joi_1.default.func()), joi_1.default.func()),
}));
exports.default = joi_1.default.object({
    serverURL: joi_1.default
        .string()
        .uri()
        .allow("")
        .custom((value, helper) => {
        const urlWithoutProtocol = value.split("//")[1];
        if (!urlWithoutProtocol) {
            return helper.message({
                custom: 'You need to include either "https://" or "http://" in your serverURL.',
            });
        }
        if (urlWithoutProtocol.indexOf("/") > -1) {
            return helper.message({
                custom: "Your serverURL cannot have a path. It can only contain a protocol, a domain, and an optional port.",
            });
        }
        return value;
    }),
    cookiePrefix: joi_1.default.string(),
    routes: joi_1.default.object({
        admin: joi_1.default.string(),
        api: joi_1.default.string(),
        graphQL: joi_1.default.string(),
        graphQLPlayground: joi_1.default.string(),
    }),
    typescript: joi_1.default.object({
        outputFile: joi_1.default.string(),
    }),
    collections: joi_1.default.array(),
    endpoints: exports.endpointsSchema,
    globals: joi_1.default.array(),
    admin: joi_1.default.object({
        user: joi_1.default.string(),
        meta: joi_1.default.object().keys({
            titleSuffix: joi_1.default.string(),
            ogImage: joi_1.default.string(),
            favicon: joi_1.default.string(),
        }),
        disable: joi_1.default.bool(),
        indexHTML: joi_1.default.string(),
        css: joi_1.default.string(),
        dateFormat: joi_1.default.string(),
        avatar: joi_1.default.alternatives().try(joi_1.default.string(), component),
        components: joi_1.default.object().keys({
            routes: joi_1.default.array().items(joi_1.default.object().keys({
                Component: component.required(),
                path: joi_1.default.string().required(),
                exact: joi_1.default.bool(),
                strict: joi_1.default.bool(),
                sensitive: joi_1.default.bool(),
            })),
            providers: joi_1.default.array().items(component),
            beforeDashboard: joi_1.default.array().items(component),
            afterDashboard: joi_1.default.array().items(component),
            beforeLogin: joi_1.default.array().items(component),
            afterLogin: joi_1.default.array().items(component),
            beforeNavLinks: joi_1.default.array().items(component),
            afterNavLinks: joi_1.default.array().items(component),
            Nav: component,
            views: joi_1.default.object({
                Dashboard: component,
                Account: component,
            }),
            graphics: joi_1.default.object({
                Icon: component,
                Logo: component,
            }),
        }),
        webpack: joi_1.default.func(),
        locale: joi_1.default.object(),
    }),
    defaultDepth: joi_1.default.number().min(0).max(30),
    maxDepth: joi_1.default.number().min(0).max(100),
    csrf: joi_1.default.array().items(joi_1.default.string().allow("")).sparse(),
    cors: [joi_1.default.string().valid("*"), joi_1.default.array().items(joi_1.default.string())],
    express: joi_1.default.object().keys({
        json: joi_1.default.object(),
        compression: joi_1.default.object(),
        middleware: joi_1.default.array().items(joi_1.default.func()),
        preMiddleware: joi_1.default.array().items(joi_1.default.func()),
        postMiddleware: joi_1.default.array().items(joi_1.default.func()),
    }),
    local: joi_1.default.boolean(),
    upload: joi_1.default.object(),
    indexSortableFields: joi_1.default.boolean(),
    rateLimit: joi_1.default.object().keys({
        window: joi_1.default.number(),
        max: joi_1.default.number(),
        trustProxy: joi_1.default.boolean(),
        skip: joi_1.default.func(),
    }),
    graphQL: joi_1.default.object().keys({
        mutations: joi_1.default.function(),
        queries: joi_1.default.function(),
        maxComplexity: joi_1.default.number(),
        disablePlaygroundInProduction: joi_1.default.boolean(),
        disable: joi_1.default.boolean(),
        schemaOutputFile: joi_1.default.string(),
    }),
    localization: joi_1.default.alternatives().try(joi_1.default.object().keys({
        locales: joi_1.default.array().items(joi_1.default.string()),
        defaultLocale: joi_1.default.string(),
        fallback: joi_1.default.boolean(),
    }), joi_1.default.boolean()),
    hooks: joi_1.default.object().keys({
        afterError: joi_1.default.func(),
    }),
    telemetry: joi_1.default.boolean(),
    plugins: joi_1.default.array().items(joi_1.default.func()),
    onInit: joi_1.default.func(),
    debug: joi_1.default.boolean(),
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2NoZW1hLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvbmZpZy9zY2hlbWEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsOENBQXNCO0FBRXRCLE1BQU0sU0FBUyxHQUFHLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sRUFBRSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0FBRWhFLFFBQUEsZUFBZSxHQUFHLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQzlDLGFBQUcsQ0FBQyxNQUFNLENBQUM7SUFDVCxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtJQUNsQixNQUFNLEVBQUUsYUFBRztTQUNSLE1BQU0sRUFBRTtTQUNSLEtBQUssQ0FDSixLQUFLLEVBQ0wsTUFBTSxFQUNOLE1BQU0sRUFDTixLQUFLLEVBQ0wsT0FBTyxFQUNQLFFBQVEsRUFDUixTQUFTLEVBQ1QsU0FBUyxDQUNWO0lBQ0gsSUFBSSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7SUFDaEIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7Q0FDM0UsQ0FBQyxDQUNILENBQUM7QUFFRixrQkFBZSxhQUFHLENBQUMsTUFBTSxDQUFDO0lBQ3hCLFNBQVMsRUFBRSxhQUFHO1NBQ1gsTUFBTSxFQUFFO1NBQ1IsR0FBRyxFQUFFO1NBQ0wsS0FBSyxDQUFDLEVBQUUsQ0FBQztTQUNULE1BQU0sQ0FBQyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsRUFBRTtRQUN4QixNQUFNLGtCQUFrQixHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFaEQsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQ3ZCLE9BQU8sTUFBTSxDQUFDLE9BQU8sQ0FBQztnQkFDcEIsTUFBTSxFQUNKLHVFQUF1RTthQUMxRSxDQUFDLENBQUM7U0FDSjtRQUVELElBQUksa0JBQWtCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ3hDLE9BQU8sTUFBTSxDQUFDLE9BQU8sQ0FBQztnQkFDcEIsTUFBTSxFQUNKLG9HQUFvRzthQUN2RyxDQUFDLENBQUM7U0FDSjtRQUVELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQyxDQUFDO0lBQ0osWUFBWSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7SUFDMUIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxNQUFNLENBQUM7UUFDakIsS0FBSyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDbkIsR0FBRyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDakIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDckIsaUJBQWlCLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtLQUNoQyxDQUFDO0lBQ0YsVUFBVSxFQUFFLGFBQUcsQ0FBQyxNQUFNLENBQUM7UUFDckIsVUFBVSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7S0FDekIsQ0FBQztJQUNGLFdBQVcsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFO0lBQ3hCLFNBQVMsRUFBRSx1QkFBZTtJQUMxQixPQUFPLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRTtJQUNwQixLQUFLLEVBQUUsYUFBRyxDQUFDLE1BQU0sQ0FBQztRQUNoQixJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUNsQixJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQztZQUN0QixXQUFXLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtZQUN6QixPQUFPLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtZQUNyQixPQUFPLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtTQUN0QixDQUFDO1FBQ0YsT0FBTyxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7UUFDbkIsU0FBUyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDdkIsR0FBRyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDakIsVUFBVSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDeEIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxFQUFFLFNBQVMsQ0FBQztRQUN2RCxVQUFVLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQztZQUM1QixNQUFNLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FDdkIsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQztnQkFDaEIsU0FBUyxFQUFFLFNBQVMsQ0FBQyxRQUFRLEVBQUU7Z0JBQy9CLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFO2dCQUM3QixLQUFLLEVBQUUsYUFBRyxDQUFDLElBQUksRUFBRTtnQkFDakIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7Z0JBQ2xCLFNBQVMsRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO2FBQ3RCLENBQUMsQ0FDSDtZQUNELFNBQVMsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUN2QyxlQUFlLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7WUFDN0MsY0FBYyxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO1lBQzVDLFdBQVcsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUN6QyxVQUFVLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7WUFDeEMsY0FBYyxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO1lBQzVDLGFBQWEsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUMzQyxHQUFHLEVBQUUsU0FBUztZQUNkLEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO2dCQUNoQixTQUFTLEVBQUUsU0FBUztnQkFDcEIsT0FBTyxFQUFFLFNBQVM7YUFDbkIsQ0FBQztZQUNGLFFBQVEsRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO2dCQUNuQixJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsU0FBUzthQUNoQixDQUFDO1NBQ0gsQ0FBQztRQUNGLE9BQU8sRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO1FBQ25CLE1BQU0sRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0tBQ3JCLENBQUM7SUFDRixZQUFZLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDO0lBQ3pDLFFBQVEsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7SUFDdEMsSUFBSSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRTtJQUN4RCxJQUFJLEVBQUUsQ0FBQyxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFDaEUsT0FBTyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7UUFDekIsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDbEIsV0FBVyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDekIsVUFBVSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3pDLGFBQWEsRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUM1QyxjQUFjLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7S0FDOUMsQ0FBQztJQUNGLEtBQUssRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFO0lBQ3BCLE1BQU0sRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0lBQ3BCLG1CQUFtQixFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7SUFDbEMsU0FBUyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7UUFDM0IsTUFBTSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDcEIsR0FBRyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDakIsVUFBVSxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7UUFDekIsSUFBSSxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7S0FDakIsQ0FBQztJQUNGLE9BQU8sRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDO1FBQ3pCLFNBQVMsRUFBRSxhQUFHLENBQUMsUUFBUSxFQUFFO1FBQ3pCLE9BQU8sRUFBRSxhQUFHLENBQUMsUUFBUSxFQUFFO1FBQ3ZCLGFBQWEsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQzNCLDZCQUE2QixFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7UUFDNUMsT0FBTyxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7UUFDdEIsZ0JBQWdCLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtLQUMvQixDQUFDO0lBQ0YsWUFBWSxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQ2xDLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7UUFDaEIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3hDLGFBQWEsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQzNCLFFBQVEsRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFO0tBQ3hCLENBQUMsRUFDRixhQUFHLENBQUMsT0FBTyxFQUFFLENBQ2Q7SUFDRCxLQUFLLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQztRQUN2QixVQUFVLEVBQUUsYUFBRyxDQUFDLElBQUksRUFBRTtLQUN2QixDQUFDO0lBQ0YsU0FBUyxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUU7SUFDeEIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3RDLE1BQU0sRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO0lBQ2xCLEtBQUssRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFO0NBQ3JCLENBQUMsQ0FBQyJ9