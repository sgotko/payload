"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.point = exports.blocks = exports.radio = exports.select = exports.array = exports.relationship = exports.upload = exports.date = exports.checkbox = exports.richText = exports.code = exports.textarea = exports.email = exports.password = exports.text = exports.number = void 0;
const defaultValue_1 = __importDefault(require("./richText/defaultValue"));
const types_1 = require("./config/types");
const canUseDOM_1 = __importDefault(require("../utilities/canUseDOM"));
const isValidID_1 = require("../utilities/isValidID");
const getIDType_1 = require("../utilities/getIDType");
const defaultMessage = 'This field is required.';
const number = (value, { required, min, max }) => {
    const parsedValue = parseFloat(value);
    if ((value && typeof parsedValue !== 'number') || (required && Number.isNaN(parsedValue)) || (value && Number.isNaN(parsedValue))) {
        return 'Please enter a valid number.';
    }
    if (typeof max === 'number' && parsedValue > max) {
        return `"${value}" is greater than the max allowed value of ${max}.`;
    }
    if (typeof min === 'number' && parsedValue < min) {
        return `"${value}" is less than the min allowed value of ${min}.`;
    }
    if (required && typeof parsedValue !== 'number') {
        return defaultMessage;
    }
    return true;
};
exports.number = number;
const text = (value, { minLength, maxLength, required }) => {
    if (value && maxLength && value.length > maxLength) {
        return `This value must be shorter than the max length of ${maxLength} characters.`;
    }
    if (value && minLength && (value === null || value === void 0 ? void 0 : value.length) < minLength) {
        return `This value must be longer than the minimum length of ${minLength} characters.`;
    }
    if (required) {
        if (typeof value !== 'string' || (value === null || value === void 0 ? void 0 : value.length) === 0) {
            return defaultMessage;
        }
    }
    return true;
};
exports.text = text;
const password = (value, { required, maxLength, minLength }) => {
    if (value && maxLength && value.length > maxLength) {
        return `This value must be shorter than the max length of ${maxLength} characters.`;
    }
    if (value && minLength && value.length < minLength) {
        return `This value must be longer than the minimum length of ${minLength} characters.`;
    }
    if (required && !value) {
        return defaultMessage;
    }
    return true;
};
exports.password = password;
const email = (value, { required }) => {
    if ((value && !/\S+@\S+\.\S+/.test(value))
        || (!value && required)) {
        return 'Please enter a valid email address.';
    }
    return true;
};
exports.email = email;
const textarea = (value, { required, maxLength, minLength, }) => {
    if (value && maxLength && value.length > maxLength) {
        return `This value must be shorter than the max length of ${maxLength} characters.`;
    }
    if (value && minLength && value.length < minLength) {
        return `This value must be longer than the minimum length of ${minLength} characters.`;
    }
    if (required && !value) {
        return defaultMessage;
    }
    return true;
};
exports.textarea = textarea;
const code = (value, { required }) => {
    if (required && value === undefined) {
        return defaultMessage;
    }
    return true;
};
exports.code = code;
const richText = (value, { required }) => {
    if (required) {
        const stringifiedDefaultValue = JSON.stringify(defaultValue_1.default);
        if (value && JSON.stringify(value) !== stringifiedDefaultValue)
            return true;
        return 'This field is required.';
    }
    return true;
};
exports.richText = richText;
const checkbox = (value, { required }) => {
    if ((value && typeof value !== 'boolean')
        || (required && typeof value !== 'boolean')) {
        return 'This field can only be equal to true or false.';
    }
    return true;
};
exports.checkbox = checkbox;
const date = (value, { required }) => {
    if (value && !isNaN(Date.parse(value.toString()))) { /* eslint-disable-line */
        return true;
    }
    if (value) {
        return `"${value}" is not a valid date.`;
    }
    if (required) {
        return defaultMessage;
    }
    return true;
};
exports.date = date;
const validateFilterOptions = async (value, { filterOptions, id, user, data, siblingData, relationTo, payload }) => {
    if (!canUseDOM_1.default && typeof filterOptions !== 'undefined' && value) {
        const options = {};
        const collections = typeof relationTo === 'string' ? [relationTo] : relationTo;
        const values = Array.isArray(value) ? value : [value];
        await Promise.all(collections.map(async (collection) => {
            const optionFilter = typeof filterOptions === 'function' ? filterOptions({
                id,
                data,
                siblingData,
                user,
                relationTo: collection,
            }) : filterOptions;
            const valueIDs = [];
            values.forEach((val) => {
                if (typeof val === 'object' && (val === null || val === void 0 ? void 0 : val.value)) {
                    valueIDs.push(val.value);
                }
                if (typeof val === 'string' || typeof val === 'number') {
                    valueIDs.push(val);
                }
            });
            const result = await payload.find({
                collection,
                depth: 0,
                where: {
                    and: [
                        { id: { in: valueIDs } },
                        optionFilter,
                    ],
                },
            });
            options[collection] = result.docs.map((doc) => doc.id);
        }));
        const invalidRelationships = values.filter((val) => {
            let collection;
            let requestedID;
            if (typeof relationTo === 'string') {
                collection = relationTo;
                if (typeof val === 'string' || typeof val === 'number') {
                    requestedID = val;
                }
            }
            if (Array.isArray(relationTo) && typeof val === 'object' && (val === null || val === void 0 ? void 0 : val.relationTo)) {
                collection = val.relationTo;
                requestedID = val.value;
            }
            return options[collection].indexOf(requestedID) === -1;
        });
        if (invalidRelationships.length > 0) {
            return invalidRelationships.reduce((err, invalid, i) => {
                return `${err} ${JSON.stringify(invalid)}${invalidRelationships.length === i + 1 ? ',' : ''} `;
            }, 'This field has the following invalid selections:');
        }
        return true;
    }
    return true;
};
const upload = (value, options) => {
    if (!value && options.required) {
        return defaultMessage;
    }
    if (!canUseDOM_1.default && typeof value !== 'undefined' && value !== null) {
        const idField = options.payload.collections[options.relationTo].config.fields.find((field) => (0, types_1.fieldAffectsData)(field) && field.name === 'id');
        const type = (0, getIDType_1.getIDType)(idField);
        if (!(0, isValidID_1.isValidID)(value, type)) {
            return 'This field is not a valid upload ID';
        }
    }
    return validateFilterOptions(value, options);
};
exports.upload = upload;
const relationship = async (value, options) => {
    if ((!value || (Array.isArray(value) && value.length === 0)) && options.required) {
        return defaultMessage;
    }
    if (!canUseDOM_1.default && typeof value !== 'undefined' && value !== null) {
        const values = Array.isArray(value) ? value : [value];
        const invalidRelationships = values.filter((val) => {
            let collection;
            let requestedID;
            if (typeof options.relationTo === 'string') {
                collection = options.relationTo;
                // custom id
                if (typeof val === 'string' || typeof val === 'number') {
                    requestedID = val;
                }
            }
            if (Array.isArray(options.relationTo) && typeof val === 'object' && (val === null || val === void 0 ? void 0 : val.relationTo)) {
                collection = val.relationTo;
                requestedID = val.value;
            }
            const idField = options.payload.collections[collection].config.fields.find((field) => (0, types_1.fieldAffectsData)(field) && field.name === 'id');
            let type;
            if (idField) {
                type = idField.type === 'number' ? 'number' : 'text';
            }
            else {
                type = 'ObjectID';
            }
            return !(0, isValidID_1.isValidID)(requestedID, type);
        });
        if (invalidRelationships.length > 0) {
            return `This field has the following invalid selections: ${invalidRelationships.map((err, invalid) => {
                return `${err} ${JSON.stringify(invalid)}`;
            }).join(', ')}`;
        }
    }
    return validateFilterOptions(value, options);
};
exports.relationship = relationship;
const array = (value, { minRows, maxRows, required }) => {
    if (minRows && value < minRows) {
        return `This field requires at least ${minRows} row(s).`;
    }
    if (maxRows && value > maxRows) {
        return `This field requires no more than ${maxRows} row(s).`;
    }
    if (!value && required) {
        return 'This field requires at least one row.';
    }
    return true;
};
exports.array = array;
const select = (value, { options, hasMany, required }) => {
    if (Array.isArray(value) && value.some((input) => !options.some((option) => (option === input || (typeof option !== 'string' && (option === null || option === void 0 ? void 0 : option.value) === input))))) {
        return 'This field has an invalid selection';
    }
    if (typeof value === 'string' && !options.some((option) => (option === value || (typeof option !== 'string' && option.value === value)))) {
        return 'This field has an invalid selection';
    }
    if (required && ((typeof value === 'undefined' || value === null) || (hasMany && Array.isArray(value) && (value === null || value === void 0 ? void 0 : value.length) === 0))) {
        return defaultMessage;
    }
    return true;
};
exports.select = select;
const radio = (value, { options, required }) => {
    const stringValue = String(value);
    if ((typeof value !== 'undefined' || !required) && (options.find((option) => String(typeof option !== 'string' && (option === null || option === void 0 ? void 0 : option.value)) === stringValue)))
        return true;
    return defaultMessage;
};
exports.radio = radio;
const blocks = (value, { maxRows, minRows, required }) => {
    if (minRows && value < minRows) {
        return `This field requires at least ${minRows} row(s).`;
    }
    if (maxRows && value > maxRows) {
        return `This field requires no more than ${maxRows} row(s).`;
    }
    if (!value && required) {
        return 'This field requires at least one row.';
    }
    return true;
};
exports.blocks = blocks;
const point = (value = ['', ''], { required }) => {
    const lng = parseFloat(String(value[0]));
    const lat = parseFloat(String(value[1]));
    if (required && ((value[0] && value[1] && typeof lng !== 'number' && typeof lat !== 'number')
        || (Number.isNaN(lng) || Number.isNaN(lat))
        || (Array.isArray(value) && value.length !== 2))) {
        return 'This field requires two numbers';
    }
    if ((value[1] && Number.isNaN(lng)) || (value[0] && Number.isNaN(lat))) {
        return 'This field has an invalid input';
    }
    return true;
};
exports.point = point;
exports.default = {
    number: exports.number,
    text: exports.text,
    password: exports.password,
    email: exports.email,
    textarea: exports.textarea,
    code: exports.code,
    richText: exports.richText,
    checkbox: exports.checkbox,
    date: exports.date,
    upload: exports.upload,
    relationship: exports.relationship,
    array: exports.array,
    select: exports.select,
    radio: exports.radio,
    blocks: exports.blocks,
    point: exports.point,
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmFsaWRhdGlvbnMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvZmllbGRzL3ZhbGlkYXRpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLDJFQUEyRDtBQUMzRCwwQ0FtQndCO0FBRXhCLHVFQUErQztBQUMvQyxzREFBbUQ7QUFDbkQsc0RBQW1EO0FBRW5ELE1BQU0sY0FBYyxHQUFHLHlCQUF5QixDQUFDO0FBRTFDLE1BQU0sTUFBTSxHQUE0QyxDQUFDLEtBQWEsRUFBRSxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRTtJQUN2RyxNQUFNLFdBQVcsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7SUFFdEMsSUFBSSxDQUFDLEtBQUssSUFBSSxPQUFPLFdBQVcsS0FBSyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFO1FBQ2pJLE9BQU8sOEJBQThCLENBQUM7S0FDdkM7SUFFRCxJQUFJLE9BQU8sR0FBRyxLQUFLLFFBQVEsSUFBSSxXQUFXLEdBQUcsR0FBRyxFQUFFO1FBQ2hELE9BQU8sSUFBSSxLQUFLLDhDQUE4QyxHQUFHLEdBQUcsQ0FBQztLQUN0RTtJQUVELElBQUksT0FBTyxHQUFHLEtBQUssUUFBUSxJQUFJLFdBQVcsR0FBRyxHQUFHLEVBQUU7UUFDaEQsT0FBTyxJQUFJLEtBQUssMkNBQTJDLEdBQUcsR0FBRyxDQUFDO0tBQ25FO0lBRUQsSUFBSSxRQUFRLElBQUksT0FBTyxXQUFXLEtBQUssUUFBUSxFQUFFO1FBQy9DLE9BQU8sY0FBYyxDQUFDO0tBQ3ZCO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDZCxDQUFDLENBQUM7QUFwQlcsUUFBQSxNQUFNLFVBb0JqQjtBQUVLLE1BQU0sSUFBSSxHQUEwQyxDQUFDLEtBQWEsRUFBRSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRTtJQUMvRyxJQUFJLEtBQUssSUFBSSxTQUFTLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxTQUFTLEVBQUU7UUFDbEQsT0FBTyxxREFBcUQsU0FBUyxjQUFjLENBQUM7S0FDckY7SUFFRCxJQUFJLEtBQUssSUFBSSxTQUFTLElBQUksQ0FBQSxLQUFLLGFBQUwsS0FBSyx1QkFBTCxLQUFLLENBQUUsTUFBTSxJQUFHLFNBQVMsRUFBRTtRQUNuRCxPQUFPLHdEQUF3RCxTQUFTLGNBQWMsQ0FBQztLQUN4RjtJQUVELElBQUksUUFBUSxFQUFFO1FBQ1osSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLElBQUksQ0FBQSxLQUFLLGFBQUwsS0FBSyx1QkFBTCxLQUFLLENBQUUsTUFBTSxNQUFLLENBQUMsRUFBRTtZQUNwRCxPQUFPLGNBQWMsQ0FBQztTQUN2QjtLQUNGO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDZCxDQUFDLENBQUM7QUFoQlcsUUFBQSxJQUFJLFFBZ0JmO0FBRUssTUFBTSxRQUFRLEdBQTBDLENBQUMsS0FBYSxFQUFFLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFO0lBQ25ILElBQUksS0FBSyxJQUFJLFNBQVMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLFNBQVMsRUFBRTtRQUNsRCxPQUFPLHFEQUFxRCxTQUFTLGNBQWMsQ0FBQztLQUNyRjtJQUVELElBQUksS0FBSyxJQUFJLFNBQVMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLFNBQVMsRUFBRTtRQUNsRCxPQUFPLHdEQUF3RCxTQUFTLGNBQWMsQ0FBQztLQUN4RjtJQUVELElBQUksUUFBUSxJQUFJLENBQUMsS0FBSyxFQUFFO1FBQ3RCLE9BQU8sY0FBYyxDQUFDO0tBQ3ZCO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDZCxDQUFDLENBQUM7QUFkVyxRQUFBLFFBQVEsWUFjbkI7QUFFSyxNQUFNLEtBQUssR0FBMkMsQ0FBQyxLQUFhLEVBQUUsRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFO0lBQzNGLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1dBQ3JDLENBQUMsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEVBQUU7UUFDekIsT0FBTyxxQ0FBcUMsQ0FBQztLQUM5QztJQUVELE9BQU8sSUFBSSxDQUFDO0FBQ2QsQ0FBQyxDQUFDO0FBUFcsUUFBQSxLQUFLLFNBT2hCO0FBRUssTUFBTSxRQUFRLEdBQThDLENBQUMsS0FBYSxFQUFFLEVBQ2pGLFFBQVEsRUFDUixTQUFTLEVBQ1QsU0FBUyxHQUNWLEVBQUUsRUFBRTtJQUNILElBQUksS0FBSyxJQUFJLFNBQVMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLFNBQVMsRUFBRTtRQUNsRCxPQUFPLHFEQUFxRCxTQUFTLGNBQWMsQ0FBQztLQUNyRjtJQUVELElBQUksS0FBSyxJQUFJLFNBQVMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLFNBQVMsRUFBRTtRQUNsRCxPQUFPLHdEQUF3RCxTQUFTLGNBQWMsQ0FBQztLQUN4RjtJQUVELElBQUksUUFBUSxJQUFJLENBQUMsS0FBSyxFQUFFO1FBQ3RCLE9BQU8sY0FBYyxDQUFDO0tBQ3ZCO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDZCxDQUFDLENBQUM7QUFsQlcsUUFBQSxRQUFRLFlBa0JuQjtBQUVLLE1BQU0sSUFBSSxHQUEwQyxDQUFDLEtBQWEsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUU7SUFDekYsSUFBSSxRQUFRLElBQUksS0FBSyxLQUFLLFNBQVMsRUFBRTtRQUNuQyxPQUFPLGNBQWMsQ0FBQztLQUN2QjtJQUVELE9BQU8sSUFBSSxDQUFDO0FBQ2QsQ0FBQyxDQUFDO0FBTlcsUUFBQSxJQUFJLFFBTWY7QUFFSyxNQUFNLFFBQVEsR0FBOEMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFO0lBQ3pGLElBQUksUUFBUSxFQUFFO1FBQ1osTUFBTSx1QkFBdUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLHNCQUFvQixDQUFDLENBQUM7UUFDckUsSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyx1QkFBdUI7WUFBRSxPQUFPLElBQUksQ0FBQztRQUM1RSxPQUFPLHlCQUF5QixDQUFDO0tBQ2xDO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDZCxDQUFDLENBQUM7QUFSVyxRQUFBLFFBQVEsWUFRbkI7QUFFSyxNQUFNLFFBQVEsR0FBOEMsQ0FBQyxLQUFjLEVBQUUsRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFO0lBQ2xHLElBQUksQ0FBQyxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssU0FBUyxDQUFDO1dBQ3BDLENBQUMsUUFBUSxJQUFJLE9BQU8sS0FBSyxLQUFLLFNBQVMsQ0FBQyxFQUFFO1FBQzdDLE9BQU8sZ0RBQWdELENBQUM7S0FDekQ7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQztBQVBXLFFBQUEsUUFBUSxZQU9uQjtBQUVLLE1BQU0sSUFBSSxHQUEwQyxDQUFDLEtBQUssRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUU7SUFDakYsSUFBSSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUseUJBQXlCO1FBQzVFLE9BQU8sSUFBSSxDQUFDO0tBQ2I7SUFFRCxJQUFJLEtBQUssRUFBRTtRQUNULE9BQU8sSUFBSSxLQUFLLHdCQUF3QixDQUFDO0tBQzFDO0lBRUQsSUFBSSxRQUFRLEVBQUU7UUFDWixPQUFPLGNBQWMsQ0FBQztLQUN2QjtJQUVELE9BQU8sSUFBSSxDQUFDO0FBQ2QsQ0FBQyxDQUFDO0FBZFcsUUFBQSxJQUFJLFFBY2Y7QUFFRixNQUFNLHFCQUFxQixHQUFhLEtBQUssRUFBRSxLQUFLLEVBQUUsRUFBRSxhQUFhLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFO0lBQzNILElBQUksQ0FBQyxtQkFBUyxJQUFJLE9BQU8sYUFBYSxLQUFLLFdBQVcsSUFBSSxLQUFLLEVBQUU7UUFDL0QsTUFBTSxPQUFPLEdBRVQsRUFBRSxDQUFDO1FBRVAsTUFBTSxXQUFXLEdBQUcsT0FBTyxVQUFVLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7UUFDL0UsTUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXRELE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxVQUFVLEVBQUUsRUFBRTtZQUNyRCxNQUFNLFlBQVksR0FBRyxPQUFPLGFBQWEsS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQztnQkFDdkUsRUFBRTtnQkFDRixJQUFJO2dCQUNKLFdBQVc7Z0JBQ1gsSUFBSTtnQkFDSixVQUFVLEVBQUUsVUFBVTthQUN2QixDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQztZQUVuQixNQUFNLFFBQVEsR0FBd0IsRUFBRSxDQUFDO1lBRXpDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtnQkFDckIsSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLEtBQUksR0FBRyxhQUFILEdBQUcsdUJBQUgsR0FBRyxDQUFFLEtBQUssQ0FBQSxFQUFFO29CQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDMUI7Z0JBRUQsSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLElBQUksT0FBTyxHQUFHLEtBQUssUUFBUSxFQUFFO29CQUN0RCxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUNwQjtZQUNILENBQUMsQ0FBQyxDQUFDO1lBRUgsTUFBTSxNQUFNLEdBQUcsTUFBTSxPQUFPLENBQUMsSUFBSSxDQUFhO2dCQUM1QyxVQUFVO2dCQUNWLEtBQUssRUFBRSxDQUFDO2dCQUNSLEtBQUssRUFBRTtvQkFDTCxHQUFHLEVBQUU7d0JBQ0gsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUU7d0JBQ3hCLFlBQVk7cUJBQ2I7aUJBQ0Y7YUFDRixDQUFDLENBQUM7WUFFSCxPQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN6RCxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRUosTUFBTSxvQkFBb0IsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7WUFDakQsSUFBSSxVQUFrQixDQUFDO1lBQ3ZCLElBQUksV0FBNEIsQ0FBQztZQUVqQyxJQUFJLE9BQU8sVUFBVSxLQUFLLFFBQVEsRUFBRTtnQkFDbEMsVUFBVSxHQUFHLFVBQVUsQ0FBQztnQkFFeEIsSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLElBQUksT0FBTyxHQUFHLEtBQUssUUFBUSxFQUFFO29CQUN0RCxXQUFXLEdBQUcsR0FBRyxDQUFDO2lCQUNuQjthQUNGO1lBRUQsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLE9BQU8sR0FBRyxLQUFLLFFBQVEsS0FBSSxHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsVUFBVSxDQUFBLEVBQUU7Z0JBQzNFLFVBQVUsR0FBRyxHQUFHLENBQUMsVUFBVSxDQUFDO2dCQUM1QixXQUFXLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQzthQUN6QjtZQUVELE9BQU8sT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUN6RCxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksb0JBQW9CLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNuQyxPQUFPLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3JELE9BQU8sR0FBRyxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsR0FBRyxvQkFBb0IsQ0FBQyxNQUFNLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQztZQUNqRyxDQUFDLEVBQUUsa0RBQWtELENBQVcsQ0FBQztTQUNsRTtRQUVELE9BQU8sSUFBSSxDQUFDO0tBQ2I7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQztBQUVLLE1BQU0sTUFBTSxHQUE0QyxDQUFDLEtBQWEsRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUN4RixJQUFJLENBQUMsS0FBSyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7UUFDOUIsT0FBTyxjQUFjLENBQUM7S0FDdkI7SUFFRCxJQUFJLENBQUMsbUJBQVMsSUFBSSxPQUFPLEtBQUssS0FBSyxXQUFXLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTtRQUNoRSxNQUFNLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUEsd0JBQWdCLEVBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQztRQUM5SSxNQUFNLElBQUksR0FBRyxJQUFBLHFCQUFTLEVBQUMsT0FBTyxDQUFDLENBQUM7UUFFaEMsSUFBSSxDQUFDLElBQUEscUJBQVMsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDM0IsT0FBTyxxQ0FBcUMsQ0FBQztTQUM5QztLQUNGO0lBRUQsT0FBTyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUM7QUFDL0MsQ0FBQyxDQUFDO0FBZlcsUUFBQSxNQUFNLFVBZWpCO0FBRUssTUFBTSxZQUFZLEdBQWtELEtBQUssRUFBRSxLQUF3QixFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ3JILElBQUksQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7UUFDaEYsT0FBTyxjQUFjLENBQUM7S0FDdkI7SUFFRCxJQUFJLENBQUMsbUJBQVMsSUFBSSxPQUFPLEtBQUssS0FBSyxXQUFXLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTtRQUNoRSxNQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFdEQsTUFBTSxvQkFBb0IsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7WUFDakQsSUFBSSxVQUFrQixDQUFDO1lBQ3ZCLElBQUksV0FBNEIsQ0FBQztZQUVqQyxJQUFJLE9BQU8sT0FBTyxDQUFDLFVBQVUsS0FBSyxRQUFRLEVBQUU7Z0JBQzFDLFVBQVUsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDO2dCQUVoQyxZQUFZO2dCQUNaLElBQUksT0FBTyxHQUFHLEtBQUssUUFBUSxJQUFJLE9BQU8sR0FBRyxLQUFLLFFBQVEsRUFBRTtvQkFDdEQsV0FBVyxHQUFHLEdBQUcsQ0FBQztpQkFDbkI7YUFDRjtZQUVELElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksT0FBTyxHQUFHLEtBQUssUUFBUSxLQUFJLEdBQUcsYUFBSCxHQUFHLHVCQUFILEdBQUcsQ0FBRSxVQUFVLENBQUEsRUFBRTtnQkFDbkYsVUFBVSxHQUFHLEdBQUcsQ0FBQyxVQUFVLENBQUM7Z0JBQzVCLFdBQVcsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO2FBQ3pCO1lBRUQsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUEsd0JBQWdCLEVBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQztZQUN0SSxJQUFJLElBQUksQ0FBQztZQUNULElBQUksT0FBTyxFQUFFO2dCQUNYLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7YUFDdEQ7aUJBQU07Z0JBQ0wsSUFBSSxHQUFHLFVBQVUsQ0FBQzthQUNuQjtZQUVELE9BQU8sQ0FBQyxJQUFBLHFCQUFTLEVBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxvQkFBb0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ25DLE9BQU8sb0RBQW9ELG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsRUFBRTtnQkFDbkcsT0FBTyxHQUFHLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7WUFDN0MsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFZLENBQUM7U0FDM0I7S0FDRjtJQUVELE9BQU8scUJBQXFCLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0FBQy9DLENBQUMsQ0FBQztBQTdDVyxRQUFBLFlBQVksZ0JBNkN2QjtBQUVLLE1BQU0sS0FBSyxHQUEyQyxDQUFDLEtBQUssRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRTtJQUNyRyxJQUFJLE9BQU8sSUFBSSxLQUFLLEdBQUcsT0FBTyxFQUFFO1FBQzlCLE9BQU8sZ0NBQWdDLE9BQU8sVUFBVSxDQUFDO0tBQzFEO0lBRUQsSUFBSSxPQUFPLElBQUksS0FBSyxHQUFHLE9BQU8sRUFBRTtRQUM5QixPQUFPLG9DQUFvQyxPQUFPLFVBQVUsQ0FBQztLQUM5RDtJQUVELElBQUksQ0FBQyxLQUFLLElBQUksUUFBUSxFQUFFO1FBQ3RCLE9BQU8sdUNBQXVDLENBQUM7S0FDaEQ7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQztBQWRXLFFBQUEsS0FBSyxTQWNoQjtBQUVLLE1BQU0sTUFBTSxHQUE0QyxDQUFDLEtBQUssRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRTtJQUN2RyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDLE1BQU0sS0FBSyxLQUFLLElBQUksQ0FBQyxPQUFPLE1BQU0sS0FBSyxRQUFRLElBQUksQ0FBQSxNQUFNLGFBQU4sTUFBTSx1QkFBTixNQUFNLENBQUUsS0FBSyxNQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1FBQzNKLE9BQU8scUNBQXFDLENBQUM7S0FDOUM7SUFFRCxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLEtBQUssSUFBSSxDQUFDLE9BQU8sTUFBTSxLQUFLLFFBQVEsSUFBSSxNQUFNLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtRQUN4SSxPQUFPLHFDQUFxQyxDQUFDO0tBQzlDO0lBRUQsSUFBSSxRQUFRLElBQUksQ0FDZCxDQUFDLE9BQU8sS0FBSyxLQUFLLFdBQVcsSUFBSSxLQUFLLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQVksYUFBWixLQUFLLHVCQUFMLEtBQUssQ0FBUyxNQUFNLE1BQUssQ0FBQyxDQUFDLENBQUMsRUFDckg7UUFDQSxPQUFPLGNBQWMsQ0FBQztLQUN2QjtJQUVELE9BQU8sSUFBSSxDQUFDO0FBQ2QsQ0FBQyxDQUFDO0FBaEJXLFFBQUEsTUFBTSxVQWdCakI7QUFFSyxNQUFNLEtBQUssR0FBMkMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRTtJQUM1RixNQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsSUFBSSxDQUFDLE9BQU8sS0FBSyxLQUFLLFdBQVcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLE9BQU8sTUFBTSxLQUFLLFFBQVEsS0FBSSxNQUFNLGFBQU4sTUFBTSx1QkFBTixNQUFNLENBQUUsS0FBSyxDQUFBLENBQUMsS0FBSyxXQUFXLENBQUMsQ0FBQztRQUFFLE9BQU8sSUFBSSxDQUFDO0lBQ2hLLE9BQU8sY0FBYyxDQUFDO0FBQ3hCLENBQUMsQ0FBQztBQUpXLFFBQUEsS0FBSyxTQUloQjtBQUVLLE1BQU0sTUFBTSxHQUEyQyxDQUFDLEtBQUssRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRTtJQUN0RyxJQUFJLE9BQU8sSUFBSSxLQUFLLEdBQUcsT0FBTyxFQUFFO1FBQzlCLE9BQU8sZ0NBQWdDLE9BQU8sVUFBVSxDQUFDO0tBQzFEO0lBRUQsSUFBSSxPQUFPLElBQUksS0FBSyxHQUFHLE9BQU8sRUFBRTtRQUM5QixPQUFPLG9DQUFvQyxPQUFPLFVBQVUsQ0FBQztLQUM5RDtJQUVELElBQUksQ0FBQyxLQUFLLElBQUksUUFBUSxFQUFFO1FBQ3RCLE9BQU8sdUNBQXVDLENBQUM7S0FDaEQ7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQztBQWRXLFFBQUEsTUFBTSxVQWNqQjtBQUVLLE1BQU0sS0FBSyxHQUEyQyxDQUFDLFFBQTRDLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRTtJQUNsSSxNQUFNLEdBQUcsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDekMsTUFBTSxHQUFHLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pDLElBQUksUUFBUSxJQUFJLENBQ2QsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLE9BQU8sR0FBRyxLQUFLLFFBQVEsSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLENBQUM7V0FDekUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7V0FDeEMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQ2hELEVBQUU7UUFDRCxPQUFPLGlDQUFpQyxDQUFDO0tBQzFDO0lBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1FBQ3RFLE9BQU8saUNBQWlDLENBQUM7S0FDMUM7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQztBQWhCVyxRQUFBLEtBQUssU0FnQmhCO0FBRUYsa0JBQWU7SUFDYixNQUFNLEVBQU4sY0FBTTtJQUNOLElBQUksRUFBSixZQUFJO0lBQ0osUUFBUSxFQUFSLGdCQUFRO0lBQ1IsS0FBSyxFQUFMLGFBQUs7SUFDTCxRQUFRLEVBQVIsZ0JBQVE7SUFDUixJQUFJLEVBQUosWUFBSTtJQUNKLFFBQVEsRUFBUixnQkFBUTtJQUNSLFFBQVEsRUFBUixnQkFBUTtJQUNSLElBQUksRUFBSixZQUFJO0lBQ0osTUFBTSxFQUFOLGNBQU07SUFDTixZQUFZLEVBQVosb0JBQVk7SUFDWixLQUFLLEVBQUwsYUFBSztJQUNMLE1BQU0sRUFBTixjQUFNO0lBQ04sS0FBSyxFQUFMLGFBQUs7SUFDTCxNQUFNLEVBQU4sY0FBTTtJQUNOLEtBQUssRUFBTCxhQUFLO0NBQ04sQ0FBQyJ9