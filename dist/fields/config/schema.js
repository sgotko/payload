"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ui = exports.date = exports.richText = exports.blocks = exports.relationship = exports.point = exports.checkbox = exports.upload = exports.array = exports.group = exports.tabs = exports.collapsible = exports.row = exports.radio = exports.select = exports.code = exports.email = exports.textarea = exports.number = exports.text = exports.idField = exports.baseField = exports.baseAdminFields = void 0;
const joi_1 = __importDefault(require("joi"));
const componentSchema_1 = require("../../utilities/componentSchema");
exports.baseAdminFields = joi_1.default.object().keys({
    description: joi_1.default.alternatives().try(joi_1.default.string(), componentSchema_1.componentSchema),
    position: joi_1.default.string().valid('sidebar'),
    width: joi_1.default.string(),
    style: joi_1.default.object().unknown(),
    className: joi_1.default.string(),
    readOnly: joi_1.default.boolean().default(false),
    initCollapsed: joi_1.default.boolean().default(false),
    hidden: joi_1.default.boolean().default(false),
    disabled: joi_1.default.boolean().default(false),
    condition: joi_1.default.func(),
    components: joi_1.default.object().keys({
        Cell: componentSchema_1.componentSchema,
        Field: componentSchema_1.componentSchema,
        Filter: componentSchema_1.componentSchema,
    }).default({}),
});
exports.baseField = joi_1.default.object().keys({
    label: joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.valid(false)),
    required: joi_1.default.boolean().default(false),
    saveToJWT: joi_1.default.boolean().default(false),
    unique: joi_1.default.boolean().default(false),
    localized: joi_1.default.boolean().default(false),
    index: joi_1.default.boolean().default(false),
    hidden: joi_1.default.boolean().default(false),
    validate: joi_1.default.func(),
    access: joi_1.default.object().keys({
        create: joi_1.default.func(),
        read: joi_1.default.func(),
        update: joi_1.default.func(),
    }),
    hooks: joi_1.default.object()
        .keys({
        beforeValidate: joi_1.default.array().items(joi_1.default.func()).default([]),
        beforeChange: joi_1.default.array().items(joi_1.default.func()).default([]),
        afterChange: joi_1.default.array().items(joi_1.default.func()).default([]),
        afterRead: joi_1.default.array().items(joi_1.default.func()).default([]),
    }).default(),
    admin: exports.baseAdminFields.default(),
}).default();
exports.idField = exports.baseField.keys({
    name: joi_1.default.string().valid('id'),
    type: joi_1.default.string().valid('text', 'number'),
    required: joi_1.default.not(false, 0).default(true),
    localized: joi_1.default.invalid(true),
});
exports.text = exports.baseField.keys({
    type: joi_1.default.string().valid('text').required(),
    name: joi_1.default.string().required(),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.func()),
    minLength: joi_1.default.number(),
    maxLength: joi_1.default.number(),
    admin: exports.baseAdminFields.keys({
        placeholder: joi_1.default.string(),
        autoComplete: joi_1.default.string(),
    }),
});
exports.number = exports.baseField.keys({
    type: joi_1.default.string().valid('number').required(),
    name: joi_1.default.string().required(),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.number(), joi_1.default.func()),
    min: joi_1.default.number(),
    max: joi_1.default.number(),
    admin: exports.baseAdminFields.keys({
        placeholder: joi_1.default.string(),
        autoComplete: joi_1.default.string(),
        step: joi_1.default.number(),
    }),
});
exports.textarea = exports.baseField.keys({
    type: joi_1.default.string().valid('textarea').required(),
    name: joi_1.default.string().required(),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.func()),
    minLength: joi_1.default.number(),
    maxLength: joi_1.default.number(),
    admin: exports.baseAdminFields.keys({
        placeholder: joi_1.default.string(),
        rows: joi_1.default.number(),
    }),
});
exports.email = exports.baseField.keys({
    type: joi_1.default.string().valid('email').required(),
    name: joi_1.default.string().required(),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.func()),
    minLength: joi_1.default.number(),
    maxLength: joi_1.default.number(),
    admin: exports.baseAdminFields.keys({
        placeholder: joi_1.default.string(),
        autoComplete: joi_1.default.string(),
    }),
});
exports.code = exports.baseField.keys({
    type: joi_1.default.string().valid('code').required(),
    name: joi_1.default.string().required(),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.func()),
    admin: exports.baseAdminFields.keys({
        language: joi_1.default.string(),
    }),
});
exports.select = exports.baseField.keys({
    type: joi_1.default.string().valid('select').required(),
    name: joi_1.default.string().required(),
    options: joi_1.default.array().min(1).items(joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.object({
        value: joi_1.default.string().required().allow(''),
        label: joi_1.default.string().required(),
    }))).required(),
    hasMany: joi_1.default.boolean().default(false),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.string().allow(''), joi_1.default.array().items(joi_1.default.string().allow('')), joi_1.default.func()),
    admin: exports.baseAdminFields.keys({
        isClearable: joi_1.default.boolean().default(false),
        isSortable: joi_1.default.boolean().default(false),
    }),
});
exports.radio = exports.baseField.keys({
    type: joi_1.default.string().valid('radio').required(),
    name: joi_1.default.string().required(),
    options: joi_1.default.array().min(1).items(joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.object({
        value: joi_1.default.string().required().allow(''),
        label: joi_1.default.string().required(),
    }))).required(),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.string().allow(''), joi_1.default.func()),
    admin: exports.baseAdminFields.keys({
        layout: joi_1.default.string().valid('vertical', 'horizontal'),
    }),
});
exports.row = exports.baseField.keys({
    type: joi_1.default.string().valid('row').required(),
    fields: joi_1.default.array().items(joi_1.default.link('#field')),
    admin: exports.baseAdminFields.default(),
});
exports.collapsible = exports.baseField.keys({
    label: joi_1.default.string().required(),
    type: joi_1.default.string().valid('collapsible').required(),
    fields: joi_1.default.array().items(joi_1.default.link('#field')),
    admin: exports.baseAdminFields.default(),
});
const tab = exports.baseField.keys({
    name: joi_1.default.string().when('localized', { is: joi_1.default.exist(), then: joi_1.default.required() }),
    localized: joi_1.default.boolean(),
    label: joi_1.default.string().required(),
    fields: joi_1.default.array().items(joi_1.default.link('#field')).required(),
    description: joi_1.default.alternatives().try(joi_1.default.string(), componentSchema_1.componentSchema),
});
exports.tabs = exports.baseField.keys({
    type: joi_1.default.string().valid('tabs').required(),
    fields: joi_1.default.forbidden(),
    localized: joi_1.default.forbidden(),
    tabs: joi_1.default.array().items(tab).required(),
    admin: exports.baseAdminFields.keys({
        description: joi_1.default.forbidden(),
    }),
});
exports.group = exports.baseField.keys({
    type: joi_1.default.string().valid('group').required(),
    name: joi_1.default.string().required(),
    fields: joi_1.default.array().items(joi_1.default.link('#field')),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.object(), joi_1.default.func()),
    admin: exports.baseAdminFields.keys({
        hideGutter: joi_1.default.boolean().default(true),
        description: joi_1.default.string(),
    }),
});
exports.array = exports.baseField.keys({
    type: joi_1.default.string().valid('array').required(),
    name: joi_1.default.string().required(),
    minRows: joi_1.default.number(),
    maxRows: joi_1.default.number(),
    fields: joi_1.default.array().items(joi_1.default.link('#field')).required(),
    labels: joi_1.default.object({
        singular: joi_1.default.string(),
        plural: joi_1.default.string(),
    }),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.array().items(joi_1.default.object()), joi_1.default.func()),
});
exports.upload = exports.baseField.keys({
    type: joi_1.default.string().valid('upload').required(),
    relationTo: joi_1.default.string().required(),
    name: joi_1.default.string().required(),
    maxDepth: joi_1.default.number(),
    filterOptions: joi_1.default.alternatives().try(joi_1.default.object(), joi_1.default.func()),
});
exports.checkbox = exports.baseField.keys({
    type: joi_1.default.string().valid('checkbox').required(),
    name: joi_1.default.string().required(),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.boolean(), joi_1.default.func()),
});
exports.point = exports.baseField.keys({
    type: joi_1.default.string().valid('point').required(),
    name: joi_1.default.string().required(),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.array().items(joi_1.default.number()).max(2).min(2), joi_1.default.func()),
});
exports.relationship = exports.baseField.keys({
    type: joi_1.default.string().valid('relationship').required(),
    hasMany: joi_1.default.boolean().default(false),
    relationTo: joi_1.default.alternatives().try(joi_1.default.string().required(), joi_1.default.array().items(joi_1.default.string())),
    name: joi_1.default.string().required(),
    maxDepth: joi_1.default.number(),
    filterOptions: joi_1.default.alternatives().try(joi_1.default.object(), joi_1.default.func()),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.func()),
    admin: exports.baseAdminFields.keys({
        isSortable: joi_1.default.boolean().default(false),
    }),
});
exports.blocks = exports.baseField.keys({
    type: joi_1.default.string().valid('blocks').required(),
    minRows: joi_1.default.number(),
    maxRows: joi_1.default.number(),
    name: joi_1.default.string().required(),
    labels: joi_1.default.object({
        singular: joi_1.default.string(),
        plural: joi_1.default.string(),
    }),
    blocks: joi_1.default.array().items(joi_1.default.object({
        slug: joi_1.default.string().required(),
        imageURL: joi_1.default.string(),
        imageAltText: joi_1.default.string(),
        labels: joi_1.default.object({
            singular: joi_1.default.string(),
            plural: joi_1.default.string(),
        }),
        fields: joi_1.default.array().items(joi_1.default.link('#field')),
    })).required(),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.array().items(joi_1.default.object()), joi_1.default.func()),
});
exports.richText = exports.baseField.keys({
    type: joi_1.default.string().valid('richText').required(),
    name: joi_1.default.string().required(),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.array().items(joi_1.default.object()), joi_1.default.func()),
    admin: exports.baseAdminFields.keys({
        placeholder: joi_1.default.string(),
        hideGutter: joi_1.default.boolean().default(true),
        elements: joi_1.default.array().items(joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.object({
            name: joi_1.default.string().required(),
            Button: componentSchema_1.componentSchema,
            Element: componentSchema_1.componentSchema,
            plugins: joi_1.default.array().items(componentSchema_1.componentSchema),
        }))),
        leaves: joi_1.default.array().items(joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.object({
            name: joi_1.default.string().required(),
            Button: componentSchema_1.componentSchema,
            Leaf: componentSchema_1.componentSchema,
            plugins: joi_1.default.array().items(componentSchema_1.componentSchema),
        }))),
        upload: joi_1.default.object({
            collections: joi_1.default.object().pattern(joi_1.default.string(), joi_1.default.object().keys({
                fields: joi_1.default.array().items(joi_1.default.link('#field')),
            })),
        }),
        link: joi_1.default.object({
            fields: joi_1.default.array().items(joi_1.default.link('#field')),
        }),
    }),
});
exports.date = exports.baseField.keys({
    type: joi_1.default.string().valid('date').required(),
    name: joi_1.default.string().required(),
    defaultValue: joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.func()),
    admin: exports.baseAdminFields.keys({
        placeholder: joi_1.default.string(),
        date: joi_1.default.object({
            displayFormat: joi_1.default.string(),
            pickerAppearance: joi_1.default.string(),
            minDate: joi_1.default.date(),
            maxDate: joi_1.default.date(),
            minTime: joi_1.default.date(),
            maxTime: joi_1.default.date(),
            timeIntervals: joi_1.default.number(),
            timeFormat: joi_1.default.string(),
            monthsToShow: joi_1.default.number(),
        }),
    }),
});
exports.ui = joi_1.default.object().keys({
    name: joi_1.default.string().required(),
    label: joi_1.default.string(),
    type: joi_1.default.string().valid('ui').required(),
    admin: joi_1.default.object().keys({
        position: joi_1.default.string().valid('sidebar'),
        width: joi_1.default.string(),
        condition: joi_1.default.func(),
        components: joi_1.default.object().keys({
            Cell: componentSchema_1.componentSchema,
            Field: componentSchema_1.componentSchema,
        }).default({}),
    }).default(),
});
const fieldSchema = joi_1.default.alternatives()
    .try(exports.text, exports.number, exports.textarea, exports.email, exports.code, exports.select, exports.group, exports.array, exports.row, exports.collapsible, exports.tabs, exports.radio, exports.relationship, exports.checkbox, exports.upload, exports.richText, exports.blocks, exports.date, exports.point, exports.ui)
    .id('field');
exports.default = fieldSchema;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2NoZW1hLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2ZpZWxkcy9jb25maWcvc2NoZW1hLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLDhDQUFzQjtBQUN0QixxRUFBa0U7QUFFckQsUUFBQSxlQUFlLEdBQUcsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQztJQUMvQyxXQUFXLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDakMsYUFBRyxDQUFDLE1BQU0sRUFBRSxFQUNaLGlDQUFlLENBQ2hCO0lBQ0QsUUFBUSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO0lBQ3ZDLEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0lBQ25CLEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsT0FBTyxFQUFFO0lBQzdCLFNBQVMsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0lBQ3ZCLFFBQVEsRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztJQUN0QyxhQUFhLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7SUFDM0MsTUFBTSxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO0lBQ3BDLFFBQVEsRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztJQUN0QyxTQUFTLEVBQUUsYUFBRyxDQUFDLElBQUksRUFBRTtJQUNyQixVQUFVLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQztRQUM1QixJQUFJLEVBQUUsaUNBQWU7UUFDckIsS0FBSyxFQUFFLGlDQUFlO1FBQ3RCLE1BQU0sRUFBRSxpQ0FBZTtLQUN4QixDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztDQUNmLENBQUMsQ0FBQztBQUVVLFFBQUEsU0FBUyxHQUFHLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7SUFDekMsS0FBSyxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQzNCLGFBQUcsQ0FBQyxNQUFNLEVBQUUsRUFDWixhQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUNqQjtJQUNELFFBQVEsRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztJQUN0QyxTQUFTLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7SUFDdkMsTUFBTSxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO0lBQ3BDLFNBQVMsRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztJQUN2QyxLQUFLLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7SUFDbkMsTUFBTSxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO0lBQ3BDLFFBQVEsRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO0lBQ3BCLE1BQU0sRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDO1FBQ3hCLE1BQU0sRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO1FBQ2xCLElBQUksRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO1FBQ2hCLE1BQU0sRUFBRSxhQUFHLENBQUMsSUFBSSxFQUFFO0tBQ25CLENBQUM7SUFDRixLQUFLLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtTQUNoQixJQUFJLENBQUM7UUFDSixjQUFjLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO1FBQ3pELFlBQVksRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7UUFDdkQsV0FBVyxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztRQUN0RCxTQUFTLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO0tBQ3JELENBQUMsQ0FBQyxPQUFPLEVBQUU7SUFDZCxLQUFLLEVBQUUsdUJBQWUsQ0FBQyxPQUFPLEVBQUU7Q0FDakMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO0FBRUEsUUFBQSxPQUFPLEdBQUcsaUJBQVMsQ0FBQyxJQUFJLENBQUM7SUFDcEMsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO0lBQzlCLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUM7SUFDMUMsUUFBUSxFQUFFLGFBQUcsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7SUFDekMsU0FBUyxFQUFFLGFBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO0NBQzdCLENBQUMsQ0FBQztBQUVVLFFBQUEsSUFBSSxHQUFHLGlCQUFTLENBQUMsSUFBSSxDQUFDO0lBQ2pDLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsRUFBRTtJQUMzQyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtJQUM3QixZQUFZLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDbEMsYUFBRyxDQUFDLE1BQU0sRUFBRSxFQUNaLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FDWDtJQUNELFNBQVMsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0lBQ3ZCLFNBQVMsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0lBQ3ZCLEtBQUssRUFBRSx1QkFBZSxDQUFDLElBQUksQ0FBQztRQUMxQixXQUFXLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUN6QixZQUFZLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtLQUMzQixDQUFDO0NBQ0gsQ0FBQyxDQUFDO0FBRVUsUUFBQSxNQUFNLEdBQUcsaUJBQVMsQ0FBQyxJQUFJLENBQUM7SUFDbkMsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxFQUFFO0lBQzdDLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFO0lBQzdCLFlBQVksRUFBRSxhQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUNsQyxhQUFHLENBQUMsTUFBTSxFQUFFLEVBQ1osYUFBRyxDQUFDLElBQUksRUFBRSxDQUNYO0lBQ0QsR0FBRyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7SUFDakIsR0FBRyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7SUFDakIsS0FBSyxFQUFFLHVCQUFlLENBQUMsSUFBSSxDQUFDO1FBQzFCLFdBQVcsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQ3pCLFlBQVksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQzFCLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0tBQ25CLENBQUM7Q0FDSCxDQUFDLENBQUM7QUFFVSxRQUFBLFFBQVEsR0FBRyxpQkFBUyxDQUFDLElBQUksQ0FBQztJQUNyQyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRLEVBQUU7SUFDL0MsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUU7SUFDN0IsWUFBWSxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQ2xDLGFBQUcsQ0FBQyxNQUFNLEVBQUUsRUFDWixhQUFHLENBQUMsSUFBSSxFQUFFLENBQ1g7SUFDRCxTQUFTLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtJQUN2QixTQUFTLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtJQUN2QixLQUFLLEVBQUUsdUJBQWUsQ0FBQyxJQUFJLENBQUM7UUFDMUIsV0FBVyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDekIsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7S0FDbkIsQ0FBQztDQUNILENBQUMsQ0FBQztBQUVVLFFBQUEsS0FBSyxHQUFHLGlCQUFTLENBQUMsSUFBSSxDQUFDO0lBQ2xDLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRTtJQUM1QyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtJQUM3QixZQUFZLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDbEMsYUFBRyxDQUFDLE1BQU0sRUFBRSxFQUNaLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FDWDtJQUNELFNBQVMsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0lBQ3ZCLFNBQVMsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0lBQ3ZCLEtBQUssRUFBRSx1QkFBZSxDQUFDLElBQUksQ0FBQztRQUMxQixXQUFXLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUN6QixZQUFZLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtLQUMzQixDQUFDO0NBQ0gsQ0FBQyxDQUFDO0FBRVUsUUFBQSxJQUFJLEdBQUcsaUJBQVMsQ0FBQyxJQUFJLENBQUM7SUFDakMsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxFQUFFO0lBQzNDLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFO0lBQzdCLFlBQVksRUFBRSxhQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUNsQyxhQUFHLENBQUMsTUFBTSxFQUFFLEVBQ1osYUFBRyxDQUFDLElBQUksRUFBRSxDQUNYO0lBQ0QsS0FBSyxFQUFFLHVCQUFlLENBQUMsSUFBSSxDQUFDO1FBQzFCLFFBQVEsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0tBQ3ZCLENBQUM7Q0FDSCxDQUFDLENBQUM7QUFFVSxRQUFBLE1BQU0sR0FBRyxpQkFBUyxDQUFDLElBQUksQ0FBQztJQUNuQyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLEVBQUU7SUFDN0MsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUU7SUFDN0IsT0FBTyxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUMvQixhQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUNwQixhQUFHLENBQUMsTUFBTSxFQUFFLEVBQ1osYUFBRyxDQUFDLE1BQU0sQ0FBQztRQUNULEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztRQUN4QyxLQUFLLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtLQUMvQixDQUFDLENBQ0gsQ0FDRixDQUFDLFFBQVEsRUFBRTtJQUNaLE9BQU8sRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztJQUNyQyxZQUFZLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDbEMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsRUFDdEIsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQ3pDLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FDWDtJQUNELEtBQUssRUFBRSx1QkFBZSxDQUFDLElBQUksQ0FBQztRQUMxQixXQUFXLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFDekMsVUFBVSxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO0tBQ3pDLENBQUM7Q0FDSCxDQUFDLENBQUM7QUFFVSxRQUFBLEtBQUssR0FBRyxpQkFBUyxDQUFDLElBQUksQ0FBQztJQUNsQyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLEVBQUU7SUFDNUMsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUU7SUFDN0IsT0FBTyxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUMvQixhQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUNwQixhQUFHLENBQUMsTUFBTSxFQUFFLEVBQ1osYUFBRyxDQUFDLE1BQU0sQ0FBQztRQUNULEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztRQUN4QyxLQUFLLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtLQUMvQixDQUFDLENBQ0gsQ0FDRixDQUFDLFFBQVEsRUFBRTtJQUNaLFlBQVksRUFBRSxhQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUNsQyxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxFQUN0QixhQUFHLENBQUMsSUFBSSxFQUFFLENBQ1g7SUFDRCxLQUFLLEVBQUUsdUJBQWUsQ0FBQyxJQUFJLENBQUM7UUFDMUIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLFlBQVksQ0FBQztLQUNyRCxDQUFDO0NBQ0gsQ0FBQyxDQUFDO0FBRVUsUUFBQSxHQUFHLEdBQUcsaUJBQVMsQ0FBQyxJQUFJLENBQUM7SUFDaEMsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFO0lBQzFDLE1BQU0sRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDN0MsS0FBSyxFQUFFLHVCQUFlLENBQUMsT0FBTyxFQUFFO0NBQ2pDLENBQUMsQ0FBQztBQUVVLFFBQUEsV0FBVyxHQUFHLGlCQUFTLENBQUMsSUFBSSxDQUFDO0lBQ3hDLEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFO0lBQzlCLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsRUFBRTtJQUNsRCxNQUFNLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzdDLEtBQUssRUFBRSx1QkFBZSxDQUFDLE9BQU8sRUFBRTtDQUNqQyxDQUFDLENBQUM7QUFFSCxNQUFNLEdBQUcsR0FBRyxpQkFBUyxDQUFDLElBQUksQ0FBQztJQUN6QixJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBRSxFQUFFLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSxhQUFHLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQztJQUMvRSxTQUFTLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRTtJQUN4QixLQUFLLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtJQUM5QixNQUFNLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFO0lBQ3hELFdBQVcsRUFBRSxhQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUNqQyxhQUFHLENBQUMsTUFBTSxFQUFFLEVBQ1osaUNBQWUsQ0FDaEI7Q0FDRixDQUFDLENBQUM7QUFFVSxRQUFBLElBQUksR0FBRyxpQkFBUyxDQUFDLElBQUksQ0FBQztJQUNqQyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLEVBQUU7SUFDM0MsTUFBTSxFQUFFLGFBQUcsQ0FBQyxTQUFTLEVBQUU7SUFDdkIsU0FBUyxFQUFFLGFBQUcsQ0FBQyxTQUFTLEVBQUU7SUFDMUIsSUFBSSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFO0lBQ3ZDLEtBQUssRUFBRSx1QkFBZSxDQUFDLElBQUksQ0FBQztRQUMxQixXQUFXLEVBQUUsYUFBRyxDQUFDLFNBQVMsRUFBRTtLQUM3QixDQUFDO0NBQ0gsQ0FBQyxDQUFDO0FBRVUsUUFBQSxLQUFLLEdBQUcsaUJBQVMsQ0FBQyxJQUFJLENBQUM7SUFDbEMsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxFQUFFO0lBQzVDLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFO0lBQzdCLE1BQU0sRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDN0MsWUFBWSxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQ2xDLGFBQUcsQ0FBQyxNQUFNLEVBQUUsRUFDWixhQUFHLENBQUMsSUFBSSxFQUFFLENBQ1g7SUFDRCxLQUFLLEVBQUUsdUJBQWUsQ0FBQyxJQUFJLENBQUM7UUFDMUIsVUFBVSxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBQ3ZDLFdBQVcsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0tBQzFCLENBQUM7Q0FDSCxDQUFDLENBQUM7QUFFVSxRQUFBLEtBQUssR0FBRyxpQkFBUyxDQUFDLElBQUksQ0FBQztJQUNsQyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLEVBQUU7SUFDNUMsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUU7SUFDN0IsT0FBTyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7SUFDckIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7SUFDckIsTUFBTSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtJQUN4RCxNQUFNLEVBQUUsYUFBRyxDQUFDLE1BQU0sQ0FBQztRQUNqQixRQUFRLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUN0QixNQUFNLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtLQUNyQixDQUFDO0lBQ0YsWUFBWSxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQ2xDLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQy9CLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FDWDtDQUNGLENBQUMsQ0FBQztBQUVVLFFBQUEsTUFBTSxHQUFHLGlCQUFTLENBQUMsSUFBSSxDQUFDO0lBQ25DLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsRUFBRTtJQUM3QyxVQUFVLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtJQUNuQyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtJQUM3QixRQUFRLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtJQUN0QixhQUFhLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDbkMsYUFBRyxDQUFDLE1BQU0sRUFBRSxFQUNaLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FDWDtDQUNGLENBQUMsQ0FBQztBQUVVLFFBQUEsUUFBUSxHQUFHLGlCQUFTLENBQUMsSUFBSSxDQUFDO0lBQ3JDLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFFBQVEsRUFBRTtJQUMvQyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtJQUM3QixZQUFZLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDbEMsYUFBRyxDQUFDLE9BQU8sRUFBRSxFQUNiLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FDWDtDQUNGLENBQUMsQ0FBQztBQUVVLFFBQUEsS0FBSyxHQUFHLGlCQUFTLENBQUMsSUFBSSxDQUFDO0lBQ2xDLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRTtJQUM1QyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtJQUM3QixZQUFZLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDbEMsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUM3QyxhQUFHLENBQUMsSUFBSSxFQUFFLENBQ1g7Q0FDRixDQUFDLENBQUM7QUFFVSxRQUFBLFlBQVksR0FBRyxpQkFBUyxDQUFDLElBQUksQ0FBQztJQUN6QyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQyxRQUFRLEVBQUU7SUFDbkQsT0FBTyxFQUFFLGFBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO0lBQ3JDLFVBQVUsRUFBRSxhQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUNoQyxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQ3ZCLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQ2hDO0lBQ0QsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUU7SUFDN0IsUUFBUSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7SUFDdEIsYUFBYSxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQ25DLGFBQUcsQ0FBQyxNQUFNLEVBQUUsRUFDWixhQUFHLENBQUMsSUFBSSxFQUFFLENBQ1g7SUFDRCxZQUFZLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDbEMsYUFBRyxDQUFDLElBQUksRUFBRSxDQUNYO0lBQ0QsS0FBSyxFQUFFLHVCQUFlLENBQUMsSUFBSSxDQUFDO1FBQzFCLFVBQVUsRUFBRSxhQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztLQUN6QyxDQUFDO0NBQ0gsQ0FBQyxDQUFDO0FBRVUsUUFBQSxNQUFNLEdBQUcsaUJBQVMsQ0FBQyxJQUFJLENBQUM7SUFDbkMsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxFQUFFO0lBQzdDLE9BQU8sRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0lBQ3JCLE9BQU8sRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0lBQ3JCLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFO0lBQzdCLE1BQU0sRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO1FBQ2pCLFFBQVEsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQ3RCLE1BQU0sRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0tBQ3JCLENBQUM7SUFDRixNQUFNLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FDdkIsYUFBRyxDQUFDLE1BQU0sQ0FBQztRQUNULElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFO1FBQzdCLFFBQVEsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQ3RCLFlBQVksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1FBQzFCLE1BQU0sRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO1lBQ2pCLFFBQVEsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1lBQ3RCLE1BQU0sRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1NBQ3JCLENBQUM7UUFDRixNQUFNLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0tBQzlDLENBQUMsQ0FDSCxDQUFDLFFBQVEsRUFBRTtJQUNaLFlBQVksRUFBRSxhQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUNsQyxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUMvQixhQUFHLENBQUMsSUFBSSxFQUFFLENBQ1g7Q0FDRixDQUFDLENBQUM7QUFFVSxRQUFBLFFBQVEsR0FBRyxpQkFBUyxDQUFDLElBQUksQ0FBQztJQUNyQyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRLEVBQUU7SUFDL0MsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUU7SUFDN0IsWUFBWSxFQUFFLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQ2xDLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQy9CLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FDWDtJQUNELEtBQUssRUFBRSx1QkFBZSxDQUFDLElBQUksQ0FBQztRQUMxQixXQUFXLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUN6QixVQUFVLEVBQUUsYUFBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7UUFDdkMsUUFBUSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQ3pCLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQ3BCLGFBQUcsQ0FBQyxNQUFNLEVBQUUsRUFDWixhQUFHLENBQUMsTUFBTSxDQUFDO1lBQ1QsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUU7WUFDN0IsTUFBTSxFQUFFLGlDQUFlO1lBQ3ZCLE9BQU8sRUFBRSxpQ0FBZTtZQUN4QixPQUFPLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxpQ0FBZSxDQUFDO1NBQzVDLENBQUMsQ0FDSCxDQUNGO1FBQ0QsTUFBTSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQ3ZCLGFBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQ3BCLGFBQUcsQ0FBQyxNQUFNLEVBQUUsRUFDWixhQUFHLENBQUMsTUFBTSxDQUFDO1lBQ1QsSUFBSSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUU7WUFDN0IsTUFBTSxFQUFFLGlDQUFlO1lBQ3ZCLElBQUksRUFBRSxpQ0FBZTtZQUNyQixPQUFPLEVBQUUsYUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxpQ0FBZSxDQUFDO1NBQzVDLENBQUMsQ0FDSCxDQUNGO1FBQ0QsTUFBTSxFQUFFLGFBQUcsQ0FBQyxNQUFNLENBQUM7WUFDakIsV0FBVyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxPQUFPLENBQUMsYUFBRyxDQUFDLE1BQU0sRUFBRSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0JBQ2hFLE1BQU0sRUFBRSxhQUFHLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLGFBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDOUMsQ0FBQyxDQUFDO1NBQ0osQ0FBQztRQUNGLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxDQUFDO1lBQ2YsTUFBTSxFQUFFLGFBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM5QyxDQUFDO0tBQ0gsQ0FBQztDQUNILENBQUMsQ0FBQztBQUVVLFFBQUEsSUFBSSxHQUFHLGlCQUFTLENBQUMsSUFBSSxDQUFDO0lBQ2pDLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsRUFBRTtJQUMzQyxJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtJQUM3QixZQUFZLEVBQUUsYUFBRyxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsQ0FDbEMsYUFBRyxDQUFDLE1BQU0sRUFBRSxFQUNaLGFBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FDWDtJQUNELEtBQUssRUFBRSx1QkFBZSxDQUFDLElBQUksQ0FBQztRQUMxQixXQUFXLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRTtRQUN6QixJQUFJLEVBQUUsYUFBRyxDQUFDLE1BQU0sQ0FBQztZQUNmLGFBQWEsRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO1lBQzNCLGdCQUFnQixFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7WUFDOUIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7WUFDbkIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7WUFDbkIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7WUFDbkIsT0FBTyxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7WUFDbkIsYUFBYSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7WUFDM0IsVUFBVSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7WUFDeEIsWUFBWSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7U0FDM0IsQ0FBQztLQUNILENBQUM7Q0FDSCxDQUFDLENBQUM7QUFFVSxRQUFBLEVBQUUsR0FBRyxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDO0lBQ2xDLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFO0lBQzdCLEtBQUssRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFO0lBQ25CLElBQUksRUFBRSxhQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsRUFBRTtJQUN6QyxLQUFLLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQztRQUN2QixRQUFRLEVBQUUsYUFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7UUFDdkMsS0FBSyxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUU7UUFDbkIsU0FBUyxFQUFFLGFBQUcsQ0FBQyxJQUFJLEVBQUU7UUFDckIsVUFBVSxFQUFFLGFBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUM7WUFDNUIsSUFBSSxFQUFFLGlDQUFlO1lBQ3JCLEtBQUssRUFBRSxpQ0FBZTtTQUN2QixDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztLQUNmLENBQUMsQ0FBQyxPQUFPLEVBQUU7Q0FDYixDQUFDLENBQUM7QUFFSCxNQUFNLFdBQVcsR0FBRyxhQUFHLENBQUMsWUFBWSxFQUFFO0tBQ25DLEdBQUcsQ0FDRixZQUFJLEVBQ0osY0FBTSxFQUNOLGdCQUFRLEVBQ1IsYUFBSyxFQUNMLFlBQUksRUFDSixjQUFNLEVBQ04sYUFBSyxFQUNMLGFBQUssRUFDTCxXQUFHLEVBQ0gsbUJBQVcsRUFDWCxZQUFJLEVBQ0osYUFBSyxFQUNMLG9CQUFZLEVBQ1osZ0JBQVEsRUFDUixjQUFNLEVBQ04sZ0JBQVEsRUFDUixjQUFNLEVBQ04sWUFBSSxFQUNKLGFBQUssRUFDTCxVQUFFLENBQ0g7S0FDQSxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7QUFFZixrQkFBZSxXQUFXLENBQUMifQ==