"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-param-reassign */
const graphql_scalars_1 = require("graphql-scalars");
const graphql_1 = require("graphql");
const formatName_1 = __importDefault(require("../../graphql/utilities/formatName"));
const buildPaginatedListType_1 = __importDefault(require("../../graphql/schema/buildPaginatedListType"));
const buildMutationInputType_1 = __importStar(require("../../graphql/schema/buildMutationInputType"));
const buildCollectionFields_1 = require("../../versions/buildCollectionFields");
const create_1 = __importDefault(require("./resolvers/create"));
const update_1 = __importDefault(require("./resolvers/update"));
const find_1 = __importDefault(require("./resolvers/find"));
const findByID_1 = __importDefault(require("./resolvers/findByID"));
const findVersionByID_1 = __importDefault(require("./resolvers/findVersionByID"));
const findVersions_1 = __importDefault(require("./resolvers/findVersions"));
const restoreVersion_1 = __importDefault(require("./resolvers/restoreVersion"));
const me_1 = __importDefault(require("../../auth/graphql/resolvers/me"));
const init_1 = __importDefault(require("../../auth/graphql/resolvers/init"));
const login_1 = __importDefault(require("../../auth/graphql/resolvers/login"));
const logout_1 = __importDefault(require("../../auth/graphql/resolvers/logout"));
const forgotPassword_1 = __importDefault(require("../../auth/graphql/resolvers/forgotPassword"));
const resetPassword_1 = __importDefault(require("../../auth/graphql/resolvers/resetPassword"));
const verifyEmail_1 = __importDefault(require("../../auth/graphql/resolvers/verifyEmail"));
const unlock_1 = __importDefault(require("../../auth/graphql/resolvers/unlock"));
const refresh_1 = __importDefault(require("../../auth/graphql/resolvers/refresh"));
const types_1 = require("../../fields/config/types");
const buildObjectType_1 = __importDefault(require("../../graphql/schema/buildObjectType"));
const buildWhereInputType_1 = __importDefault(require("../../graphql/schema/buildWhereInputType"));
const delete_1 = __importDefault(require("./resolvers/delete"));
function initCollectionsGraphQL(payload) {
    Object.keys(payload.collections).forEach((slug) => {
        const collection = payload.collections[slug];
        const { config: { labels: { singular, plural, }, fields, timestamps, versions, }, } = collection;
        const singularLabel = (0, formatName_1.default)(singular);
        let pluralLabel = (0, formatName_1.default)(plural);
        // For collections named 'Media' or similar,
        // there is a possibility that the singular name
        // will equal the plural name. Append `all` to the beginning
        // of potential conflicts
        if (singularLabel === pluralLabel) {
            pluralLabel = `all${singularLabel}`;
        }
        collection.graphQL = {};
        const idField = fields.find((field) => (0, types_1.fieldAffectsData)(field) && field.name === 'id');
        const idType = (0, buildMutationInputType_1.getCollectionIDType)(collection.config);
        const baseFields = {};
        const whereInputFields = [
            ...fields,
        ];
        if (!idField) {
            baseFields.id = { type: idType };
            whereInputFields.push({
                name: 'id',
                type: 'text',
            });
        }
        if (timestamps) {
            baseFields.createdAt = {
                type: new graphql_1.GraphQLNonNull(graphql_scalars_1.DateTimeResolver),
            };
            baseFields.updatedAt = {
                type: new graphql_1.GraphQLNonNull(graphql_scalars_1.DateTimeResolver),
            };
            whereInputFields.push({
                name: 'createdAt',
                label: 'Created At',
                type: 'date',
            });
            whereInputFields.push({
                name: 'updatedAt',
                label: 'Updated At',
                type: 'date',
            });
        }
        const forceNullableObjectType = Boolean(versions === null || versions === void 0 ? void 0 : versions.drafts);
        collection.graphQL.type = (0, buildObjectType_1.default)({
            payload,
            name: singularLabel,
            parentName: singularLabel,
            fields,
            baseFields,
            forceNullable: forceNullableObjectType,
        });
        collection.graphQL.whereInputType = (0, buildWhereInputType_1.default)(singularLabel, whereInputFields, singularLabel);
        if (collection.config.auth && !collection.config.auth.disableLocalStrategy) {
            fields.push({
                name: 'password',
                label: 'Password',
                type: 'text',
                required: true,
            });
        }
        collection.graphQL.mutationInputType = new graphql_1.GraphQLNonNull((0, buildMutationInputType_1.default)(payload, singularLabel, fields, singularLabel));
        collection.graphQL.updateMutationInputType = new graphql_1.GraphQLNonNull((0, buildMutationInputType_1.default)(payload, `${singularLabel}Update`, fields.filter((field) => !((0, types_1.fieldAffectsData)(field) && field.name === 'id')), `${singularLabel}Update`, true));
        payload.Query.fields[singularLabel] = {
            type: collection.graphQL.type,
            args: {
                id: { type: new graphql_1.GraphQLNonNull(idType) },
                draft: { type: graphql_1.GraphQLBoolean },
                ...(payload.config.localization ? {
                    locale: { type: payload.types.localeInputType },
                    fallbackLocale: { type: payload.types.fallbackLocaleInputType },
                } : {}),
            },
            resolve: (0, findByID_1.default)(collection),
        };
        payload.Query.fields[pluralLabel] = {
            type: (0, buildPaginatedListType_1.default)(pluralLabel, collection.graphQL.type),
            args: {
                where: { type: collection.graphQL.whereInputType },
                draft: { type: graphql_1.GraphQLBoolean },
                ...(payload.config.localization ? {
                    locale: { type: payload.types.localeInputType },
                    fallbackLocale: { type: payload.types.fallbackLocaleInputType },
                } : {}),
                page: { type: graphql_1.GraphQLInt },
                limit: { type: graphql_1.GraphQLInt },
                sort: { type: graphql_1.GraphQLString },
            },
            resolve: (0, find_1.default)(collection),
        };
        payload.Mutation.fields[`create${singularLabel}`] = {
            type: collection.graphQL.type,
            args: {
                data: { type: collection.graphQL.mutationInputType },
                draft: { type: graphql_1.GraphQLBoolean },
                ...(payload.config.localization ? {
                    locale: { type: payload.types.localeInputType },
                } : {}),
            },
            resolve: (0, create_1.default)(collection),
        };
        payload.Mutation.fields[`update${singularLabel}`] = {
            type: collection.graphQL.type,
            args: {
                id: { type: new graphql_1.GraphQLNonNull(idType) },
                data: { type: collection.graphQL.updateMutationInputType },
                draft: { type: graphql_1.GraphQLBoolean },
                autosave: { type: graphql_1.GraphQLBoolean },
                ...(payload.config.localization ? {
                    locale: { type: payload.types.localeInputType },
                } : {}),
            },
            resolve: (0, update_1.default)(collection),
        };
        payload.Mutation.fields[`delete${singularLabel}`] = {
            type: collection.graphQL.type,
            args: {
                id: { type: new graphql_1.GraphQLNonNull(idType) },
            },
            resolve: (0, delete_1.default)(collection),
        };
        if (collection.config.versions) {
            const versionCollectionFields = [
                ...(0, buildCollectionFields_1.buildVersionCollectionFields)(collection.config),
                {
                    name: 'id',
                    type: 'text',
                },
                {
                    name: 'createdAt',
                    label: 'Created At',
                    type: 'date',
                },
                {
                    name: 'updatedAt',
                    label: 'Updated At',
                    type: 'date',
                },
            ];
            collection.graphQL.versionType = (0, buildObjectType_1.default)({
                payload,
                name: `${singularLabel}Version`,
                fields: versionCollectionFields,
                parentName: `${singularLabel}Version`,
                forceNullable: forceNullableObjectType,
            });
            payload.Query.fields[`version${(0, formatName_1.default)(singularLabel)}`] = {
                type: collection.graphQL.versionType,
                args: {
                    id: { type: graphql_1.GraphQLString },
                    ...(payload.config.localization ? {
                        locale: { type: payload.types.localeInputType },
                        fallbackLocale: { type: payload.types.fallbackLocaleInputType },
                    } : {}),
                },
                resolve: (0, findVersionByID_1.default)(collection),
            };
            payload.Query.fields[`versions${pluralLabel}`] = {
                type: (0, buildPaginatedListType_1.default)(`versions${(0, formatName_1.default)(pluralLabel)}`, collection.graphQL.versionType),
                args: {
                    where: {
                        type: (0, buildWhereInputType_1.default)(`versions${singularLabel}`, versionCollectionFields, `versions${singularLabel}`),
                    },
                    ...(payload.config.localization ? {
                        locale: { type: payload.types.localeInputType },
                        fallbackLocale: { type: payload.types.fallbackLocaleInputType },
                    } : {}),
                    page: { type: graphql_1.GraphQLInt },
                    limit: { type: graphql_1.GraphQLInt },
                    sort: { type: graphql_1.GraphQLString },
                },
                resolve: (0, findVersions_1.default)(collection),
            };
            payload.Mutation.fields[`restoreVersion${(0, formatName_1.default)(singularLabel)}`] = {
                type: collection.graphQL.type,
                args: {
                    id: { type: graphql_1.GraphQLString },
                },
                resolve: (0, restoreVersion_1.default)(collection),
            };
        }
        if (collection.config.auth) {
            const authFields = collection.config.auth.disableLocalStrategy ? [] : [{
                    name: 'email',
                    type: 'email',
                    required: true,
                }];
            collection.graphQL.JWT = (0, buildObjectType_1.default)({
                payload,
                name: (0, formatName_1.default)(`${slug}JWT`),
                fields: [
                    ...collection.config.fields.filter((field) => (0, types_1.fieldAffectsData)(field) && field.saveToJWT),
                    ...authFields,
                    {
                        name: 'collection',
                        type: 'text',
                        required: true,
                    }
                ],
                parentName: (0, formatName_1.default)(`${slug}JWT`),
            });
            payload.Query.fields[`me${singularLabel}`] = {
                type: new graphql_1.GraphQLObjectType({
                    name: (0, formatName_1.default)(`${slug}Me`),
                    fields: {
                        token: {
                            type: graphql_1.GraphQLString,
                        },
                        user: {
                            type: collection.graphQL.type,
                        },
                        exp: {
                            type: graphql_1.GraphQLInt,
                        },
                        collection: {
                            type: graphql_1.GraphQLString,
                        },
                    },
                }),
                resolve: (0, me_1.default)(collection),
            };
            payload.Query.fields[`initialized${singularLabel}`] = {
                type: graphql_1.GraphQLBoolean,
                resolve: (0, init_1.default)(collection),
            };
            payload.Mutation.fields[`refreshToken${singularLabel}`] = {
                type: new graphql_1.GraphQLObjectType({
                    name: (0, formatName_1.default)(`${slug}Refreshed${singularLabel}`),
                    fields: {
                        user: {
                            type: collection.graphQL.JWT,
                        },
                        refreshedToken: {
                            type: graphql_1.GraphQLString,
                        },
                        exp: {
                            type: graphql_1.GraphQLInt,
                        },
                    },
                }),
                args: {
                    token: { type: graphql_1.GraphQLString },
                },
                resolve: (0, refresh_1.default)(collection),
            };
            payload.Mutation.fields[`logout${singularLabel}`] = {
                type: graphql_1.GraphQLString,
                resolve: (0, logout_1.default)(collection),
            };
            if (!collection.config.auth.disableLocalStrategy) {
                if (collection.config.auth.maxLoginAttempts > 0) {
                    payload.Mutation.fields[`unlock${singularLabel}`] = {
                        type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLBoolean),
                        args: {
                            email: { type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLString) },
                        },
                        resolve: (0, unlock_1.default)(collection),
                    };
                }
                payload.Mutation.fields[`login${singularLabel}`] = {
                    type: new graphql_1.GraphQLObjectType({
                        name: (0, formatName_1.default)(`${slug}LoginResult`),
                        fields: {
                            token: {
                                type: graphql_1.GraphQLString,
                            },
                            user: {
                                type: collection.graphQL.type,
                            },
                            exp: {
                                type: graphql_1.GraphQLInt,
                            },
                        },
                    }),
                    args: {
                        email: { type: graphql_1.GraphQLString },
                        password: { type: graphql_1.GraphQLString },
                    },
                    resolve: (0, login_1.default)(collection),
                };
                payload.Mutation.fields[`forgotPassword${singularLabel}`] = {
                    type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLBoolean),
                    args: {
                        email: { type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLString) },
                        disableEmail: { type: graphql_1.GraphQLBoolean },
                        expiration: { type: graphql_1.GraphQLInt },
                    },
                    resolve: (0, forgotPassword_1.default)(collection),
                };
                payload.Mutation.fields[`resetPassword${singularLabel}`] = {
                    type: new graphql_1.GraphQLObjectType({
                        name: (0, formatName_1.default)(`${slug}ResetPassword`),
                        fields: {
                            token: { type: graphql_1.GraphQLString },
                            user: { type: collection.graphQL.type },
                        },
                    }),
                    args: {
                        token: { type: graphql_1.GraphQLString },
                        password: { type: graphql_1.GraphQLString },
                    },
                    resolve: (0, resetPassword_1.default)(collection),
                };
                payload.Mutation.fields[`verifyEmail${singularLabel}`] = {
                    type: graphql_1.GraphQLBoolean,
                    args: {
                        token: { type: graphql_1.GraphQLString },
                    },
                    resolve: (0, verifyEmail_1.default)(collection),
                };
            }
        }
    });
}
exports.default = initCollectionsGraphQL;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb2xsZWN0aW9ucy9ncmFwaHFsL2luaXQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHNDQUFzQztBQUN0QyxxREFBbUQ7QUFDbkQscUNBTWlCO0FBRWpCLG9GQUE0RDtBQUM1RCx5R0FBaUY7QUFDakYsc0dBQTBHO0FBQzFHLGdGQUFvRjtBQUNwRixnRUFBZ0Q7QUFDaEQsZ0VBQWdEO0FBQ2hELDREQUE0QztBQUM1QyxvRUFBb0Q7QUFDcEQsa0ZBQWtFO0FBQ2xFLDRFQUE0RDtBQUM1RCxnRkFBZ0U7QUFDaEUseUVBQWlEO0FBQ2pELDZFQUFxRDtBQUNyRCwrRUFBdUQ7QUFDdkQsaUZBQXlEO0FBQ3pELGlHQUF5RTtBQUN6RSwrRkFBdUU7QUFDdkUsMkZBQW1FO0FBQ25FLGlGQUF5RDtBQUN6RCxtRkFBMkQ7QUFFM0QscURBQW9FO0FBQ3BFLDJGQUF5RjtBQUN6RixtR0FBMkU7QUFDM0UsZ0VBQW1EO0FBRW5ELFNBQVMsc0JBQXNCLENBQUMsT0FBZ0I7SUFDOUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7UUFDaEQsTUFBTSxVQUFVLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3QyxNQUFNLEVBQ0osTUFBTSxFQUFFLEVBQ04sTUFBTSxFQUFFLEVBQ04sUUFBUSxFQUNSLE1BQU0sR0FDUCxFQUNELE1BQU0sRUFDTixVQUFVLEVBQ1YsUUFBUSxHQUNULEdBQ0YsR0FBRyxVQUFVLENBQUM7UUFFZixNQUFNLGFBQWEsR0FBRyxJQUFBLG9CQUFVLEVBQUMsUUFBUSxDQUFDLENBQUM7UUFDM0MsSUFBSSxXQUFXLEdBQUcsSUFBQSxvQkFBVSxFQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXJDLDRDQUE0QztRQUM1QyxnREFBZ0Q7UUFDaEQsNERBQTREO1FBQzVELHlCQUF5QjtRQUV6QixJQUFJLGFBQWEsS0FBSyxXQUFXLEVBQUU7WUFDakMsV0FBVyxHQUFHLE1BQU0sYUFBYSxFQUFFLENBQUM7U0FDckM7UUFFRCxVQUFVLENBQUMsT0FBTyxHQUFHLEVBQVMsQ0FBQztRQUUvQixNQUFNLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFBLHdCQUFnQixFQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUM7UUFDdkYsTUFBTSxNQUFNLEdBQUcsSUFBQSw0Q0FBbUIsRUFBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFdEQsTUFBTSxVQUFVLEdBQXFCLEVBQUUsQ0FBQztRQUV4QyxNQUFNLGdCQUFnQixHQUFHO1lBQ3ZCLEdBQUcsTUFBTTtTQUNWLENBQUM7UUFFRixJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ1osVUFBVSxDQUFDLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsQ0FBQztZQUNqQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7Z0JBQ3BCLElBQUksRUFBRSxJQUFJO2dCQUNWLElBQUksRUFBRSxNQUFNO2FBQ2IsQ0FBQyxDQUFDO1NBQ0o7UUFFRCxJQUFJLFVBQVUsRUFBRTtZQUNkLFVBQVUsQ0FBQyxTQUFTLEdBQUc7Z0JBQ3JCLElBQUksRUFBRSxJQUFJLHdCQUFjLENBQUMsa0NBQWdCLENBQUM7YUFDM0MsQ0FBQztZQUVGLFVBQVUsQ0FBQyxTQUFTLEdBQUc7Z0JBQ3JCLElBQUksRUFBRSxJQUFJLHdCQUFjLENBQUMsa0NBQWdCLENBQUM7YUFDM0MsQ0FBQztZQUVGLGdCQUFnQixDQUFDLElBQUksQ0FBQztnQkFDcEIsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLEtBQUssRUFBRSxZQUFZO2dCQUNuQixJQUFJLEVBQUUsTUFBTTthQUNiLENBQUMsQ0FBQztZQUVILGdCQUFnQixDQUFDLElBQUksQ0FBQztnQkFDcEIsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLEtBQUssRUFBRSxZQUFZO2dCQUNuQixJQUFJLEVBQUUsTUFBTTthQUNiLENBQUMsQ0FBQztTQUNKO1FBRUQsTUFBTSx1QkFBdUIsR0FBRyxPQUFPLENBQUMsUUFBUSxhQUFSLFFBQVEsdUJBQVIsUUFBUSxDQUFFLE1BQU0sQ0FBQyxDQUFDO1FBRTFELFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUEseUJBQWUsRUFBQztZQUN4QyxPQUFPO1lBQ1AsSUFBSSxFQUFFLGFBQWE7WUFDbkIsVUFBVSxFQUFFLGFBQWE7WUFDekIsTUFBTTtZQUNOLFVBQVU7WUFDVixhQUFhLEVBQUUsdUJBQXVCO1NBQ3ZDLENBQUMsQ0FBQztRQUVILFVBQVUsQ0FBQyxPQUFPLENBQUMsY0FBYyxHQUFHLElBQUEsNkJBQW1CLEVBQ3JELGFBQWEsRUFDYixnQkFBZ0IsRUFDaEIsYUFBYSxDQUNkLENBQUM7UUFFRixJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDMUUsTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDVixJQUFJLEVBQUUsVUFBVTtnQkFDaEIsS0FBSyxFQUFFLFVBQVU7Z0JBQ2pCLElBQUksRUFBRSxNQUFNO2dCQUNaLFFBQVEsRUFBRSxJQUFJO2FBQ2YsQ0FBQyxDQUFDO1NBQ0o7UUFFRCxVQUFVLENBQUMsT0FBTyxDQUFDLGlCQUFpQixHQUFHLElBQUksd0JBQWMsQ0FBQyxJQUFBLGdDQUFzQixFQUM5RSxPQUFPLEVBQ1AsYUFBYSxFQUNiLE1BQU0sRUFDTixhQUFhLENBQ2QsQ0FBQyxDQUFDO1FBRUgsVUFBVSxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLHdCQUFjLENBQUMsSUFBQSxnQ0FBc0IsRUFDcEYsT0FBTyxFQUNQLEdBQUcsYUFBYSxRQUFRLEVBQ3hCLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFBLHdCQUFnQixFQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsRUFDM0UsR0FBRyxhQUFhLFFBQVEsRUFDeEIsSUFBSSxDQUNMLENBQUMsQ0FBQztRQUVILE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxHQUFHO1lBQ3BDLElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDN0IsSUFBSSxFQUFFO2dCQUNKLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLHdCQUFjLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ3hDLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSx3QkFBYyxFQUFFO2dCQUMvQixHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO29CQUNoQyxNQUFNLEVBQUUsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxlQUFlLEVBQUU7b0JBQy9DLGNBQWMsRUFBRSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLHVCQUF1QixFQUFFO2lCQUNoRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDUjtZQUNELE9BQU8sRUFBRSxJQUFBLGtCQUFnQixFQUFDLFVBQVUsQ0FBQztTQUN0QyxDQUFDO1FBRUYsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUc7WUFDbEMsSUFBSSxFQUFFLElBQUEsZ0NBQXNCLEVBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ2xFLElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUU7Z0JBQ2xELEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSx3QkFBYyxFQUFFO2dCQUMvQixHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO29CQUNoQyxNQUFNLEVBQUUsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxlQUFlLEVBQUU7b0JBQy9DLGNBQWMsRUFBRSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLHVCQUF1QixFQUFFO2lCQUNoRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ1AsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLG9CQUFVLEVBQUU7Z0JBQzFCLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSxvQkFBVSxFQUFFO2dCQUMzQixJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTthQUM5QjtZQUNELE9BQU8sRUFBRSxJQUFBLGNBQVksRUFBQyxVQUFVLENBQUM7U0FDbEMsQ0FBQztRQUVGLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsYUFBYSxFQUFFLENBQUMsR0FBRztZQUNsRCxJQUFJLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJO1lBQzdCLElBQUksRUFBRTtnQkFDSixJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRTtnQkFDcEQsS0FBSyxFQUFFLEVBQUUsSUFBSSxFQUFFLHdCQUFjLEVBQUU7Z0JBQy9CLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7b0JBQ2hDLE1BQU0sRUFBRSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLGVBQWUsRUFBRTtpQkFDaEQsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ1I7WUFDRCxPQUFPLEVBQUUsSUFBQSxnQkFBYyxFQUFDLFVBQVUsQ0FBQztTQUNwQyxDQUFDO1FBRUYsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxhQUFhLEVBQUUsQ0FBQyxHQUFHO1lBQ2xELElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDN0IsSUFBSSxFQUFFO2dCQUNKLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLHdCQUFjLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ3hDLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLHVCQUF1QixFQUFFO2dCQUMxRCxLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsd0JBQWMsRUFBRTtnQkFDL0IsUUFBUSxFQUFFLEVBQUUsSUFBSSxFQUFFLHdCQUFjLEVBQUU7Z0JBQ2xDLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7b0JBQ2hDLE1BQU0sRUFBRSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLGVBQWUsRUFBRTtpQkFDaEQsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ1I7WUFDRCxPQUFPLEVBQUUsSUFBQSxnQkFBYyxFQUFDLFVBQVUsQ0FBQztTQUNwQyxDQUFDO1FBRUYsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxhQUFhLEVBQUUsQ0FBQyxHQUFHO1lBQ2xELElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUk7WUFDN0IsSUFBSSxFQUFFO2dCQUNKLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLHdCQUFjLENBQUMsTUFBTSxDQUFDLEVBQUU7YUFDekM7WUFDRCxPQUFPLEVBQUUsSUFBQSxnQkFBaUIsRUFBQyxVQUFVLENBQUM7U0FDdkMsQ0FBQztRQUVGLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7WUFDOUIsTUFBTSx1QkFBdUIsR0FBWTtnQkFDdkMsR0FBRyxJQUFBLG9EQUE0QixFQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUM7Z0JBQ2xEO29CQUNFLElBQUksRUFBRSxJQUFJO29CQUNWLElBQUksRUFBRSxNQUFNO2lCQUNiO2dCQUNEO29CQUNFLElBQUksRUFBRSxXQUFXO29CQUNqQixLQUFLLEVBQUUsWUFBWTtvQkFDbkIsSUFBSSxFQUFFLE1BQU07aUJBQ2I7Z0JBQ0Q7b0JBQ0UsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLEtBQUssRUFBRSxZQUFZO29CQUNuQixJQUFJLEVBQUUsTUFBTTtpQkFDYjthQUNGLENBQUM7WUFFRixVQUFVLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFBLHlCQUFlLEVBQUM7Z0JBQy9DLE9BQU87Z0JBQ1AsSUFBSSxFQUFFLEdBQUcsYUFBYSxTQUFTO2dCQUMvQixNQUFNLEVBQUUsdUJBQXVCO2dCQUMvQixVQUFVLEVBQUUsR0FBRyxhQUFhLFNBQVM7Z0JBQ3JDLGFBQWEsRUFBRSx1QkFBdUI7YUFDdkMsQ0FBQyxDQUFDO1lBRUgsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxJQUFBLG9CQUFVLEVBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxHQUFHO2dCQUM1RCxJQUFJLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxXQUFXO2dCQUNwQyxJQUFJLEVBQUU7b0JBQ0osRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLHVCQUFhLEVBQUU7b0JBQzNCLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7d0JBQ2hDLE1BQU0sRUFBRSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLGVBQWUsRUFBRTt3QkFDL0MsY0FBYyxFQUFFLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsdUJBQXVCLEVBQUU7cUJBQ2hFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDUjtnQkFDRCxPQUFPLEVBQUUsSUFBQSx5QkFBdUIsRUFBQyxVQUFVLENBQUM7YUFDN0MsQ0FBQztZQUNGLE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFdBQVcsV0FBVyxFQUFFLENBQUMsR0FBRztnQkFDL0MsSUFBSSxFQUFFLElBQUEsZ0NBQXNCLEVBQUMsV0FBVyxJQUFBLG9CQUFVLEVBQUMsV0FBVyxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztnQkFDbEcsSUFBSSxFQUFFO29CQUNKLEtBQUssRUFBRTt3QkFDTCxJQUFJLEVBQUUsSUFBQSw2QkFBbUIsRUFDdkIsV0FBVyxhQUFhLEVBQUUsRUFDMUIsdUJBQXVCLEVBQ3ZCLFdBQVcsYUFBYSxFQUFFLENBQzNCO3FCQUNGO29CQUNELEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7d0JBQ2hDLE1BQU0sRUFBRSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLGVBQWUsRUFBRTt3QkFDL0MsY0FBYyxFQUFFLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsdUJBQXVCLEVBQUU7cUJBQ2hFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDUCxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsb0JBQVUsRUFBRTtvQkFDMUIsS0FBSyxFQUFFLEVBQUUsSUFBSSxFQUFFLG9CQUFVLEVBQUU7b0JBQzNCLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO2lCQUM5QjtnQkFDRCxPQUFPLEVBQUUsSUFBQSxzQkFBb0IsRUFBQyxVQUFVLENBQUM7YUFDMUMsQ0FBQztZQUNGLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLGlCQUFpQixJQUFBLG9CQUFVLEVBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxHQUFHO2dCQUN0RSxJQUFJLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJO2dCQUM3QixJQUFJLEVBQUU7b0JBQ0osRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLHVCQUFhLEVBQUU7aUJBQzVCO2dCQUNELE9BQU8sRUFBRSxJQUFBLHdCQUFzQixFQUFDLFVBQVUsQ0FBQzthQUM1QyxDQUFDO1NBQ0g7UUFFRCxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFO1lBQzFCLE1BQU0sVUFBVSxHQUFZLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzlFLElBQUksRUFBRSxPQUFPO29CQUNiLElBQUksRUFBRSxPQUFPO29CQUNiLFFBQVEsRUFBRSxJQUFJO2lCQUNmLENBQUMsQ0FBQTtZQUNGLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUEseUJBQWUsRUFBQztnQkFDdkMsT0FBTztnQkFDUCxJQUFJLEVBQUUsSUFBQSxvQkFBVSxFQUFDLEdBQUcsSUFBSSxLQUFLLENBQUM7Z0JBQzlCLE1BQU0sRUFBRTtvQkFDTixHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBQSx3QkFBZ0IsRUFBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsU0FBUyxDQUFDO29CQUN6RixHQUFHLFVBQVU7b0JBQ2I7d0JBQ0UsSUFBSSxFQUFFLFlBQVk7d0JBQ2xCLElBQUksRUFBRSxNQUFNO3dCQUNaLFFBQVEsRUFBRSxJQUFJO3FCQUNmO2lCQUNGO2dCQUNELFVBQVUsRUFBRSxJQUFBLG9CQUFVLEVBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQzthQUNyQyxDQUFDLENBQUM7WUFFSCxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLGFBQWEsRUFBRSxDQUFDLEdBQUc7Z0JBQzNDLElBQUksRUFBRSxJQUFJLDJCQUFpQixDQUFDO29CQUMxQixJQUFJLEVBQUUsSUFBQSxvQkFBVSxFQUFDLEdBQUcsSUFBSSxJQUFJLENBQUM7b0JBQzdCLE1BQU0sRUFBRTt3QkFDTixLQUFLLEVBQUU7NEJBQ0wsSUFBSSxFQUFFLHVCQUFhO3lCQUNwQjt3QkFDRCxJQUFJLEVBQUU7NEJBQ0osSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSTt5QkFDOUI7d0JBQ0QsR0FBRyxFQUFFOzRCQUNILElBQUksRUFBRSxvQkFBVTt5QkFDakI7d0JBQ0QsVUFBVSxFQUFFOzRCQUNWLElBQUksRUFBRSx1QkFBYTt5QkFDcEI7cUJBQ0Y7aUJBQ0YsQ0FBQztnQkFDRixPQUFPLEVBQUUsSUFBQSxZQUFFLEVBQUMsVUFBVSxDQUFDO2FBQ3hCLENBQUM7WUFFRixPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxjQUFjLGFBQWEsRUFBRSxDQUFDLEdBQUc7Z0JBQ3BELElBQUksRUFBRSx3QkFBYztnQkFDcEIsT0FBTyxFQUFFLElBQUEsY0FBSSxFQUFDLFVBQVUsQ0FBQzthQUMxQixDQUFDO1lBRUYsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsZUFBZSxhQUFhLEVBQUUsQ0FBQyxHQUFHO2dCQUN4RCxJQUFJLEVBQUUsSUFBSSwyQkFBaUIsQ0FBQztvQkFDMUIsSUFBSSxFQUFFLElBQUEsb0JBQVUsRUFBQyxHQUFHLElBQUksWUFBWSxhQUFhLEVBQUUsQ0FBQztvQkFDcEQsTUFBTSxFQUFFO3dCQUNOLElBQUksRUFBRTs0QkFDSixJQUFJLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHO3lCQUM3Qjt3QkFDRCxjQUFjLEVBQUU7NEJBQ2QsSUFBSSxFQUFFLHVCQUFhO3lCQUNwQjt3QkFDRCxHQUFHLEVBQUU7NEJBQ0gsSUFBSSxFQUFFLG9CQUFVO3lCQUNqQjtxQkFDRjtpQkFDRixDQUFDO2dCQUNGLElBQUksRUFBRTtvQkFDSixLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtpQkFDL0I7Z0JBQ0QsT0FBTyxFQUFFLElBQUEsaUJBQU8sRUFBQyxVQUFVLENBQUM7YUFDN0IsQ0FBQztZQUVGLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsYUFBYSxFQUFFLENBQUMsR0FBRztnQkFDbEQsSUFBSSxFQUFFLHVCQUFhO2dCQUNuQixPQUFPLEVBQUUsSUFBQSxnQkFBTSxFQUFDLFVBQVUsQ0FBQzthQUM1QixDQUFDO1lBRUYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFO2dCQUNoRCxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsRUFBRTtvQkFDL0MsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxhQUFhLEVBQUUsQ0FBQyxHQUFHO3dCQUNsRCxJQUFJLEVBQUUsSUFBSSx3QkFBYyxDQUFDLHdCQUFjLENBQUM7d0JBQ3hDLElBQUksRUFBRTs0QkFDSixLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSx3QkFBYyxDQUFDLHVCQUFhLENBQUMsRUFBRTt5QkFDbkQ7d0JBQ0QsT0FBTyxFQUFFLElBQUEsZ0JBQU0sRUFBQyxVQUFVLENBQUM7cUJBQzVCLENBQUM7aUJBQ0g7Z0JBRUQsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxhQUFhLEVBQUUsQ0FBQyxHQUFHO29CQUNqRCxJQUFJLEVBQUUsSUFBSSwyQkFBaUIsQ0FBQzt3QkFDMUIsSUFBSSxFQUFFLElBQUEsb0JBQVUsRUFBQyxHQUFHLElBQUksYUFBYSxDQUFDO3dCQUN0QyxNQUFNLEVBQUU7NEJBQ04sS0FBSyxFQUFFO2dDQUNMLElBQUksRUFBRSx1QkFBYTs2QkFDcEI7NEJBQ0QsSUFBSSxFQUFFO2dDQUNKLElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUk7NkJBQzlCOzRCQUNELEdBQUcsRUFBRTtnQ0FDSCxJQUFJLEVBQUUsb0JBQVU7NkJBQ2pCO3lCQUNGO3FCQUNGLENBQUM7b0JBQ0YsSUFBSSxFQUFFO3dCQUNKLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO3dCQUM5QixRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtxQkFDbEM7b0JBQ0QsT0FBTyxFQUFFLElBQUEsZUFBSyxFQUFDLFVBQVUsQ0FBQztpQkFDM0IsQ0FBQztnQkFFRixPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsYUFBYSxFQUFFLENBQUMsR0FBRztvQkFDMUQsSUFBSSxFQUFFLElBQUksd0JBQWMsQ0FBQyx3QkFBYyxDQUFDO29CQUN4QyxJQUFJLEVBQUU7d0JBQ0osS0FBSyxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksd0JBQWMsQ0FBQyx1QkFBYSxDQUFDLEVBQUU7d0JBQ2xELFlBQVksRUFBRSxFQUFFLElBQUksRUFBRSx3QkFBYyxFQUFFO3dCQUN0QyxVQUFVLEVBQUUsRUFBRSxJQUFJLEVBQUUsb0JBQVUsRUFBRTtxQkFDakM7b0JBQ0QsT0FBTyxFQUFFLElBQUEsd0JBQWMsRUFBQyxVQUFVLENBQUM7aUJBQ3BDLENBQUM7Z0JBRUYsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLGFBQWEsRUFBRSxDQUFDLEdBQUc7b0JBQ3pELElBQUksRUFBRSxJQUFJLDJCQUFpQixDQUFDO3dCQUMxQixJQUFJLEVBQUUsSUFBQSxvQkFBVSxFQUFDLEdBQUcsSUFBSSxlQUFlLENBQUM7d0JBQ3hDLE1BQU0sRUFBRTs0QkFDTixLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTs0QkFDOUIsSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFO3lCQUN4QztxQkFDRixDQUFDO29CQUNGLElBQUksRUFBRTt3QkFDSixLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTt3QkFDOUIsUUFBUSxFQUFFLEVBQUUsSUFBSSxFQUFFLHVCQUFhLEVBQUU7cUJBQ2xDO29CQUNELE9BQU8sRUFBRSxJQUFBLHVCQUFhLEVBQUMsVUFBVSxDQUFDO2lCQUNuQyxDQUFDO2dCQUVGLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLGNBQWMsYUFBYSxFQUFFLENBQUMsR0FBRztvQkFDdkQsSUFBSSxFQUFFLHdCQUFjO29CQUNwQixJQUFJLEVBQUU7d0JBQ0osS0FBSyxFQUFFLEVBQUUsSUFBSSxFQUFFLHVCQUFhLEVBQUU7cUJBQy9CO29CQUNELE9BQU8sRUFBRSxJQUFBLHFCQUFXLEVBQUMsVUFBVSxDQUFDO2lCQUNqQyxDQUFDO2FBQ0g7U0FDRjtJQUNILENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQztBQUVELGtCQUFlLHNCQUFzQixDQUFDIn0=