"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const getFileByPath_1 = __importDefault(require("../../../uploads/getFileByPath"));
const create_1 = __importDefault(require("../create"));
const dataloader_1 = require("../../dataloader");
async function createLocal(payload, options) {
    var _a, _b, _c;
    const { collection: collectionSlug, depth, locale, fallbackLocale, data, user, overrideAccess = true, disableVerificationEmail, showHiddenFields, filePath, file, overwriteExistingFiles = false, req = {}, draft, } = options;
    const collection = payload.collections[collectionSlug];
    req.payloadAPI = 'local';
    req.locale = locale || (req === null || req === void 0 ? void 0 : req.locale) || (((_a = payload === null || payload === void 0 ? void 0 : payload.config) === null || _a === void 0 ? void 0 : _a.localization) ? (_c = (_b = payload === null || payload === void 0 ? void 0 : payload.config) === null || _b === void 0 ? void 0 : _b.localization) === null || _c === void 0 ? void 0 : _c.defaultLocale : null);
    req.fallbackLocale = fallbackLocale || (req === null || req === void 0 ? void 0 : req.fallbackLocale) || null;
    req.payload = payload;
    req.files = {
        file: (file !== null && file !== void 0 ? file : (await (0, getFileByPath_1.default)(filePath))),
    };
    if (typeof user !== 'undefined')
        req.user = user;
    if (!req.payloadDataLoader)
        req.payloadDataLoader = (0, dataloader_1.getDataLoader)(req);
    return (0, create_1.default)({
        depth,
        data,
        collection,
        overrideAccess,
        disableVerificationEmail,
        showHiddenFields,
        overwriteExistingFiles,
        draft,
        req,
    });
}
exports.default = createLocal;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlYXRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2NvbGxlY3Rpb25zL29wZXJhdGlvbnMvbG9jYWwvY3JlYXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBSUEsbUZBQTJEO0FBQzNELHVEQUErQjtBQUMvQixpREFBaUQ7QUFxQmxDLEtBQUssVUFBVSxXQUFXLENBQVUsT0FBZ0IsRUFBRSxPQUFtQjs7SUFDdEYsTUFBTSxFQUNKLFVBQVUsRUFBRSxjQUFjLEVBQzFCLEtBQUssRUFDTCxNQUFNLEVBQ04sY0FBYyxFQUNkLElBQUksRUFDSixJQUFJLEVBQ0osY0FBYyxHQUFHLElBQUksRUFDckIsd0JBQXdCLEVBQ3hCLGdCQUFnQixFQUNoQixRQUFRLEVBQ1IsSUFBSSxFQUNKLHNCQUFzQixHQUFHLEtBQUssRUFDOUIsR0FBRyxHQUFHLEVBQW9CLEVBQzFCLEtBQUssR0FDTixHQUFHLE9BQU8sQ0FBQztJQUVaLE1BQU0sVUFBVSxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUM7SUFFdkQsR0FBRyxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUM7SUFDekIsR0FBRyxDQUFDLE1BQU0sR0FBRyxNQUFNLEtBQUksR0FBRyxhQUFILEdBQUcsdUJBQUgsR0FBRyxDQUFFLE1BQU0sQ0FBQSxJQUFJLENBQUMsQ0FBQSxNQUFBLE9BQU8sYUFBUCxPQUFPLHVCQUFQLE9BQU8sQ0FBRSxNQUFNLDBDQUFFLFlBQVksRUFBQyxDQUFDLENBQUMsTUFBQSxNQUFBLE9BQU8sYUFBUCxPQUFPLHVCQUFQLE9BQU8sQ0FBRSxNQUFNLDBDQUFFLFlBQVksMENBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1SCxHQUFHLENBQUMsY0FBYyxHQUFHLGNBQWMsS0FBSSxHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsY0FBYyxDQUFBLElBQUksSUFBSSxDQUFDO0lBQ25FLEdBQUcsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0lBQ3RCLEdBQUcsQ0FBQyxLQUFLLEdBQUc7UUFDVixJQUFJLEVBQUUsQ0FBQyxJQUFJLGFBQUosSUFBSSxjQUFKLElBQUksR0FBSSxDQUFDLE1BQU0sSUFBQSx1QkFBYSxFQUFDLFFBQVEsQ0FBQyxDQUFDLENBQWlCO0tBQ2hFLENBQUM7SUFFRixJQUFJLE9BQU8sSUFBSSxLQUFLLFdBQVc7UUFBRSxHQUFHLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUVqRCxJQUFJLENBQUMsR0FBRyxDQUFDLGlCQUFpQjtRQUFFLEdBQUcsQ0FBQyxpQkFBaUIsR0FBRyxJQUFBLDBCQUFhLEVBQUMsR0FBRyxDQUFDLENBQUM7SUFFdkUsT0FBTyxJQUFBLGdCQUFNLEVBQUM7UUFDWixLQUFLO1FBQ0wsSUFBSTtRQUNKLFVBQVU7UUFDVixjQUFjO1FBQ2Qsd0JBQXdCO1FBQ3hCLGdCQUFnQjtRQUNoQixzQkFBc0I7UUFDdEIsS0FBSztRQUNMLEdBQUc7S0FDSixDQUFDLENBQUM7QUFDTCxDQUFDO0FBM0NELDhCQTJDQyJ9