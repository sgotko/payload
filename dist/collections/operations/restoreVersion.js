"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-underscore-dangle */
const http_status_1 = __importDefault(require("http-status"));
const errors_1 = require("../../errors");
const executeAccess_1 = __importDefault(require("../../auth/executeAccess"));
const types_1 = require("../../auth/types");
const sanitizeInternalFields_1 = __importDefault(require("../../utilities/sanitizeInternalFields"));
const afterChange_1 = require("../../fields/hooks/afterChange");
const afterRead_1 = require("../../fields/hooks/afterRead");
async function restoreVersion(args) {
    const { collection: { Model, config: collectionConfig, }, id, overrideAccess = false, showHiddenFields, depth, req: { locale, payload, }, req, } = args;
    if (!id) {
        throw new errors_1.APIError('Missing ID of version to restore.', http_status_1.default.BAD_REQUEST);
    }
    // /////////////////////////////////////
    // Retrieve original raw version
    // /////////////////////////////////////
    const VersionModel = payload.versions[collectionConfig.slug];
    let rawVersion = await VersionModel.findOne({
        _id: id,
    });
    if (!rawVersion) {
        throw new errors_1.NotFound();
    }
    rawVersion = rawVersion.toJSON({ virtuals: true });
    const parentDocID = rawVersion.parent;
    // /////////////////////////////////////
    // Access
    // /////////////////////////////////////
    const accessResults = !overrideAccess ? await (0, executeAccess_1.default)({ req, id: parentDocID }, collectionConfig.access.update) : true;
    const hasWherePolicy = (0, types_1.hasWhereAccessResult)(accessResults);
    // /////////////////////////////////////
    // Retrieve document
    // /////////////////////////////////////
    const queryToBuild = {
        where: {
            and: [
                {
                    id: {
                        equals: parentDocID,
                    },
                },
            ],
        },
    };
    if ((0, types_1.hasWhereAccessResult)(accessResults)) {
        queryToBuild.where.and.push(accessResults);
    }
    const query = await Model.buildQuery(queryToBuild, locale);
    const doc = await Model.findOne(query);
    if (!doc && !hasWherePolicy)
        throw new errors_1.NotFound();
    if (!doc && hasWherePolicy)
        throw new errors_1.Forbidden();
    // /////////////////////////////////////
    // fetch previousDoc
    // /////////////////////////////////////
    const previousDoc = await payload.findByID({
        collection: collectionConfig.slug,
        id: parentDocID,
        depth,
    });
    // /////////////////////////////////////
    // Update
    // /////////////////////////////////////
    let result = await Model.findByIdAndUpdate({ _id: parentDocID }, rawVersion.version, { new: true });
    result = result.toJSON({ virtuals: true });
    // custom id type reset
    result.id = result._id;
    result = JSON.stringify(result);
    result = JSON.parse(result);
    result = (0, sanitizeInternalFields_1.default)(result);
    // /////////////////////////////////////
    // afterRead - Fields
    // /////////////////////////////////////
    result = await (0, afterRead_1.afterRead)({
        depth,
        doc: result,
        entityConfig: collectionConfig,
        req,
        overrideAccess,
        showHiddenFields,
    });
    // /////////////////////////////////////
    // afterRead - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterRead.reduce(async (priorHook, hook) => {
        await priorHook;
        result = await hook({
            req,
            doc: result,
        }) || result;
    }, Promise.resolve());
    // /////////////////////////////////////
    // afterChange - Fields
    // /////////////////////////////////////
    result = await (0, afterChange_1.afterChange)({
        data: result,
        doc: result,
        previousDoc,
        entityConfig: collectionConfig,
        operation: 'update',
        req,
    });
    // /////////////////////////////////////
    // afterChange - Collection
    // /////////////////////////////////////
    await collectionConfig.hooks.afterChange.reduce(async (priorHook, hook) => {
        await priorHook;
        result = await hook({
            doc: result,
            req,
            previousDoc,
            operation: 'update',
        }) || result;
    }, Promise.resolve());
    return result;
}
exports.default = restoreVersion;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzdG9yZVZlcnNpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY29sbGVjdGlvbnMvb3BlcmF0aW9ucy9yZXN0b3JlVmVyc2lvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLHlDQUF5QztBQUN6Qyw4REFBcUM7QUFHckMseUNBQTZEO0FBQzdELDZFQUFxRDtBQUNyRCw0Q0FBd0Q7QUFFeEQsb0dBQTRFO0FBQzVFLGdFQUE2RDtBQUM3RCw0REFBeUQ7QUFhekQsS0FBSyxVQUFVLGNBQWMsQ0FBNkIsSUFBZTtJQUN2RSxNQUFNLEVBQ0osVUFBVSxFQUFFLEVBQ1YsS0FBSyxFQUNMLE1BQU0sRUFBRSxnQkFBZ0IsR0FDekIsRUFDRCxFQUFFLEVBQ0YsY0FBYyxHQUFHLEtBQUssRUFDdEIsZ0JBQWdCLEVBQ2hCLEtBQUssRUFDTCxHQUFHLEVBQUUsRUFDSCxNQUFNLEVBQ04sT0FBTyxHQUNSLEVBQ0QsR0FBRyxHQUNKLEdBQUcsSUFBSSxDQUFDO0lBRVQsSUFBSSxDQUFDLEVBQUUsRUFBRTtRQUNQLE1BQU0sSUFBSSxpQkFBUSxDQUFDLG1DQUFtQyxFQUFFLHFCQUFVLENBQUMsV0FBVyxDQUFDLENBQUM7S0FDakY7SUFFRCx3Q0FBd0M7SUFDeEMsZ0NBQWdDO0lBQ2hDLHdDQUF3QztJQUV4QyxNQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO0lBRTdELElBQUksVUFBVSxHQUFHLE1BQU0sWUFBWSxDQUFDLE9BQU8sQ0FBQztRQUMxQyxHQUFHLEVBQUUsRUFBRTtLQUNSLENBQUMsQ0FBQztJQUVILElBQUksQ0FBQyxVQUFVLEVBQUU7UUFDZixNQUFNLElBQUksaUJBQVEsRUFBRSxDQUFDO0tBQ3RCO0lBRUQsVUFBVSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUVuRCxNQUFNLFdBQVcsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDO0lBRXRDLHdDQUF3QztJQUN4QyxTQUFTO0lBQ1Qsd0NBQXdDO0lBRXhDLE1BQU0sYUFBYSxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUEsdUJBQWEsRUFBQyxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsV0FBVyxFQUFFLEVBQUUsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDN0gsTUFBTSxjQUFjLEdBQUcsSUFBQSw0QkFBb0IsRUFBQyxhQUFhLENBQUMsQ0FBQztJQUUzRCx3Q0FBd0M7SUFDeEMsb0JBQW9CO0lBQ3BCLHdDQUF3QztJQUV4QyxNQUFNLFlBQVksR0FBcUI7UUFDckMsS0FBSyxFQUFFO1lBQ0wsR0FBRyxFQUFFO2dCQUNIO29CQUNFLEVBQUUsRUFBRTt3QkFDRixNQUFNLEVBQUUsV0FBVztxQkFDcEI7aUJBQ0Y7YUFDRjtTQUNGO0tBQ0YsQ0FBQztJQUVGLElBQUksSUFBQSw0QkFBb0IsRUFBQyxhQUFhLENBQUMsRUFBRTtRQUN0QyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7S0FDekQ7SUFFRCxNQUFNLEtBQUssR0FBRyxNQUFNLEtBQUssQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBRTNELE1BQU0sR0FBRyxHQUFHLE1BQU0sS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUV2QyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYztRQUFFLE1BQU0sSUFBSSxpQkFBUSxFQUFFLENBQUM7SUFDbEQsSUFBSSxDQUFDLEdBQUcsSUFBSSxjQUFjO1FBQUUsTUFBTSxJQUFJLGtCQUFTLEVBQUUsQ0FBQztJQUVsRCx3Q0FBd0M7SUFDeEMsb0JBQW9CO0lBQ3BCLHdDQUF3QztJQUV4QyxNQUFNLFdBQVcsR0FBRyxNQUFNLE9BQU8sQ0FBQyxRQUFRLENBQUM7UUFDekMsVUFBVSxFQUFFLGdCQUFnQixDQUFDLElBQUk7UUFDakMsRUFBRSxFQUFFLFdBQVc7UUFDZixLQUFLO0tBQ04sQ0FBQyxDQUFDO0lBRUgsd0NBQXdDO0lBQ3hDLFNBQVM7SUFDVCx3Q0FBd0M7SUFFeEMsSUFBSSxNQUFNLEdBQUcsTUFBTSxLQUFLLENBQUMsaUJBQWlCLENBQ3hDLEVBQUUsR0FBRyxFQUFFLFdBQVcsRUFBRSxFQUNwQixVQUFVLENBQUMsT0FBTyxFQUNsQixFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FDZCxDQUFDO0lBRUYsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUUzQyx1QkFBdUI7SUFDdkIsTUFBTSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDO0lBQ3ZCLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzVCLE1BQU0sR0FBRyxJQUFBLGdDQUFzQixFQUFDLE1BQU0sQ0FBQyxDQUFDO0lBRXhDLHdDQUF3QztJQUN4QyxxQkFBcUI7SUFDckIsd0NBQXdDO0lBRXhDLE1BQU0sR0FBRyxNQUFNLElBQUEscUJBQVMsRUFBQztRQUN2QixLQUFLO1FBQ0wsR0FBRyxFQUFFLE1BQU07UUFDWCxZQUFZLEVBQUUsZ0JBQWdCO1FBQzlCLEdBQUc7UUFDSCxjQUFjO1FBQ2QsZ0JBQWdCO0tBQ2pCLENBQUMsQ0FBQztJQUVILHdDQUF3QztJQUN4Qyx5QkFBeUI7SUFDekIsd0NBQXdDO0lBRXhDLE1BQU0sZ0JBQWdCLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFBRTtRQUN0RSxNQUFNLFNBQVMsQ0FBQztRQUVoQixNQUFNLEdBQUcsTUFBTSxJQUFJLENBQUM7WUFDbEIsR0FBRztZQUNILEdBQUcsRUFBRSxNQUFNO1NBQ1osQ0FBQyxJQUFJLE1BQU0sQ0FBQztJQUNmLENBQUMsRUFBRSxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUV0Qix3Q0FBd0M7SUFDeEMsdUJBQXVCO0lBQ3ZCLHdDQUF3QztJQUV4QyxNQUFNLEdBQUcsTUFBTSxJQUFBLHlCQUFXLEVBQUM7UUFDekIsSUFBSSxFQUFFLE1BQU07UUFDWixHQUFHLEVBQUUsTUFBTTtRQUNYLFdBQVc7UUFDWCxZQUFZLEVBQUUsZ0JBQWdCO1FBQzlCLFNBQVMsRUFBRSxRQUFRO1FBQ25CLEdBQUc7S0FDSixDQUFDLENBQUM7SUFFSCx3Q0FBd0M7SUFDeEMsMkJBQTJCO0lBQzNCLHdDQUF3QztJQUV4QyxNQUFNLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUU7UUFDeEUsTUFBTSxTQUFTLENBQUM7UUFFaEIsTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDO1lBQ2xCLEdBQUcsRUFBRSxNQUFNO1lBQ1gsR0FBRztZQUNILFdBQVc7WUFDWCxTQUFTLEVBQUUsUUFBUTtTQUNwQixDQUFDLElBQUksTUFBTSxDQUFDO0lBQ2YsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBRXRCLE9BQU8sTUFBTSxDQUFDO0FBQ2hCLENBQUM7QUFFRCxrQkFBZSxjQUFjLENBQUMifQ==