"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable class-methods-use-this */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-use-before-define */
const mongoose_1 = require("mongoose");
const types_1 = require("../fields/config/types");
const formatBaseSchema = (field, buildSchemaOptions) => {
    const schema = {
        unique: (!buildSchemaOptions.disableUnique && field.unique) || false,
        required: false,
        index: field.index || (!buildSchemaOptions.disableUnique && field.unique) || false,
    };
    if ((schema.unique && (field.localized || buildSchemaOptions.draftsEnabled))) {
        schema.sparse = true;
    }
    return schema;
};
const localizeSchema = (entity, schema, localization) => {
    if ((0, types_1.fieldIsLocalized)(entity) && localization && Array.isArray(localization.locales)) {
        return {
            type: localization.locales.reduce((localeSchema, locale) => ({
                ...localeSchema,
                [locale]: schema,
            }), {
                _id: false,
            }),
            localized: true,
        };
    }
    return schema;
};
const buildSchema = (config, configFields, buildSchemaOptions = {}) => {
    const { allowIDField, options } = buildSchemaOptions;
    let fields = {};
    let schemaFields = configFields;
    if (!allowIDField) {
        const idField = schemaFields.find((field) => (0, types_1.fieldAffectsData)(field) && field.name === 'id');
        if (idField) {
            fields = {
                _id: idField.type === 'number' ? Number : String,
            };
            schemaFields = schemaFields.filter((field) => !((0, types_1.fieldAffectsData)(field) && field.name === 'id'));
        }
    }
    const schema = new mongoose_1.Schema(fields, options);
    schemaFields.forEach((field) => {
        if (!(0, types_1.fieldIsPresentationalOnly)(field)) {
            const addFieldSchema = fieldToSchemaMap[field.type];
            if (addFieldSchema) {
                addFieldSchema(field, schema, config, buildSchemaOptions);
            }
        }
    });
    return schema;
};
const fieldToSchemaMap = {
    number: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = { ...formatBaseSchema(field, buildSchemaOptions), type: Number };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    text: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = { ...formatBaseSchema(field, buildSchemaOptions), type: String };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    email: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = { ...formatBaseSchema(field, buildSchemaOptions), type: String };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    textarea: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = { ...formatBaseSchema(field, buildSchemaOptions), type: String };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    richText: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = { ...formatBaseSchema(field, buildSchemaOptions), type: mongoose_1.Schema.Types.Mixed };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    code: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = { ...formatBaseSchema(field, buildSchemaOptions), type: String };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    point: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = {
            type: {
                type: String,
                enum: ['Point'],
            },
            coordinates: {
                type: [Number],
                required: false,
                default: field.defaultValue || undefined,
            },
        };
        if (buildSchemaOptions.disableUnique && field.unique && field.localized) {
            baseSchema.coordinates.sparse = true;
        }
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
        if (field.index === true || field.index === undefined) {
            const indexOptions = {};
            if (!buildSchemaOptions.disableUnique && field.unique) {
                indexOptions.sparse = true;
                indexOptions.unique = true;
            }
            if (field.localized && config.localization) {
                config.localization.locales.forEach((locale) => {
                    schema.index({ [`${field.name}.${locale}`]: '2dsphere' }, indexOptions);
                });
            }
            else {
                schema.index({ [field.name]: '2dsphere' }, indexOptions);
            }
        }
    },
    radio: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = {
            ...formatBaseSchema(field, buildSchemaOptions),
            type: String,
            enum: field.options.map((option) => {
                if (typeof option === 'object')
                    return option.value;
                return option;
            }),
        };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    checkbox: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = { ...formatBaseSchema(field, buildSchemaOptions), type: Boolean };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    date: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = { ...formatBaseSchema(field, buildSchemaOptions), type: Date };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    upload: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = {
            ...formatBaseSchema(field, buildSchemaOptions),
            type: mongoose_1.Schema.Types.Mixed,
            ref: field.relationTo,
        };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    relationship: (field, schema, config, buildSchemaOptions) => {
        const hasManyRelations = Array.isArray(field.relationTo);
        let schemaToReturn = {};
        if (field.localized && config.localization) {
            schemaToReturn = {
                type: config.localization.locales.reduce((locales, locale) => {
                    let localeSchema = {};
                    if (hasManyRelations) {
                        localeSchema._id = false;
                        localeSchema.value = {
                            type: mongoose_1.Schema.Types.Mixed,
                            refPath: `${field.name}.${locale}.relationTo`,
                        };
                        localeSchema.relationTo = { type: String, enum: field.relationTo };
                    }
                    else {
                        localeSchema = {
                            ...formatBaseSchema(field, buildSchemaOptions),
                            type: mongoose_1.Schema.Types.Mixed,
                            ref: field.relationTo,
                        };
                    }
                    return {
                        ...locales,
                        [locale]: field.hasMany ? [localeSchema] : localeSchema,
                    };
                }, {}),
                localized: true,
            };
        }
        else if (hasManyRelations) {
            schemaToReturn._id = false;
            schemaToReturn.value = {
                type: mongoose_1.Schema.Types.Mixed,
                refPath: `${field.name}.relationTo`,
            };
            schemaToReturn.relationTo = { type: String, enum: field.relationTo };
            if (field.hasMany)
                schemaToReturn = [schemaToReturn];
        }
        else {
            schemaToReturn = {
                ...formatBaseSchema(field, buildSchemaOptions),
                type: mongoose_1.Schema.Types.Mixed,
                ref: field.relationTo,
            };
            if (field.hasMany)
                schemaToReturn = [schemaToReturn];
        }
        schema.add({
            [field.name]: schemaToReturn,
        });
    },
    row: (field, schema, config, buildSchemaOptions) => {
        field.fields.forEach((subField) => {
            const addFieldSchema = fieldToSchemaMap[subField.type];
            if (addFieldSchema) {
                addFieldSchema(subField, schema, config, buildSchemaOptions);
            }
        });
    },
    collapsible: (field, schema, config, buildSchemaOptions) => {
        field.fields.forEach((subField) => {
            const addFieldSchema = fieldToSchemaMap[subField.type];
            if (addFieldSchema) {
                addFieldSchema(subField, schema, config, buildSchemaOptions);
            }
        });
    },
    tabs: (field, schema, config, buildSchemaOptions) => {
        field.tabs.forEach((tab) => {
            if ((0, types_1.tabHasName)(tab)) {
                const baseSchema = {
                    type: buildSchema(config, tab.fields, {
                        options: {
                            _id: false,
                            id: false,
                        },
                        disableUnique: buildSchemaOptions.disableUnique,
                    }),
                };
                schema.add({
                    [tab.name]: localizeSchema(tab, baseSchema, config.localization),
                });
            }
            else {
                tab.fields.forEach((subField) => {
                    const addFieldSchema = fieldToSchemaMap[subField.type];
                    if (addFieldSchema) {
                        addFieldSchema(subField, schema, config, buildSchemaOptions);
                    }
                });
            }
        });
    },
    array: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = {
            ...formatBaseSchema(field, buildSchemaOptions),
            type: [buildSchema(config, field.fields, {
                    options: { _id: false, id: false },
                    allowIDField: true,
                    disableUnique: buildSchemaOptions.disableUnique,
                })],
        };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    group: (field, schema, config, buildSchemaOptions) => {
        const formattedBaseSchema = formatBaseSchema(field, buildSchemaOptions);
        const baseSchema = {
            ...formattedBaseSchema,
            type: buildSchema(config, field.fields, {
                options: {
                    _id: false,
                    id: false,
                },
                disableUnique: buildSchemaOptions.disableUnique,
            }),
        };
        schema.add({
            [field.name]: localizeSchema(field, baseSchema, config.localization),
        });
    },
    select: (field, schema, config, buildSchemaOptions) => {
        const baseSchema = {
            ...formatBaseSchema(field, buildSchemaOptions),
            type: String,
            enum: field.options.map((option) => {
                if (typeof option === 'object')
                    return option.value;
                return option;
            }),
        };
        const schemaToReturn = localizeSchema(field, baseSchema, config.localization);
        schema.add({
            [field.name]: field.hasMany ? [schemaToReturn] : schemaToReturn,
        });
    },
    blocks: (field, schema, config, buildSchemaOptions) => {
        const fieldSchema = [new mongoose_1.Schema({}, { _id: false, discriminatorKey: 'blockType' })];
        schema.add({
            [field.name]: localizeSchema(field, fieldSchema, config.localization),
        });
        field.blocks.forEach((blockItem) => {
            const blockSchema = new mongoose_1.Schema({}, { _id: false, id: false });
            blockItem.fields.forEach((blockField) => {
                const addFieldSchema = fieldToSchemaMap[blockField.type];
                if (addFieldSchema) {
                    addFieldSchema(blockField, blockSchema, config, buildSchemaOptions);
                }
            });
            if (field.localized && config.localization) {
                config.localization.locales.forEach((locale) => {
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore Possible incorrect typing in mongoose types, this works
                    schema.path(`${field.name}.${locale}`).discriminator(blockItem.slug, blockSchema);
                });
            }
            else {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore Possible incorrect typing in mongoose types, this works
                schema.path(field.name).discriminator(blockItem.slug, blockSchema);
            }
        });
    },
};
exports.default = buildSchema;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVpbGRTY2hlbWEuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvbW9uZ29vc2UvYnVpbGRTY2hlbWEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzREFBc0Q7QUFDdEQsMkNBQTJDO0FBQzNDLDREQUE0RDtBQUM1RCx5Q0FBeUM7QUFDekMsdUNBQWtGO0FBRWxGLGtEQTRCZ0M7QUFZaEMsTUFBTSxnQkFBZ0IsR0FBRyxDQUFDLEtBQXlCLEVBQUUsa0JBQXNDLEVBQUUsRUFBRTtJQUM3RixNQUFNLE1BQU0sR0FBK0I7UUFDekMsTUFBTSxFQUFFLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEtBQUs7UUFDcEUsUUFBUSxFQUFFLEtBQUs7UUFDZixLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsa0JBQWtCLENBQUMsYUFBYSxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxLQUFLO0tBQ25GLENBQUM7SUFFRixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRTtRQUM1RSxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztLQUN0QjtJQUVELE9BQU8sTUFBTSxDQUFDO0FBQ2hCLENBQUMsQ0FBQztBQUVGLE1BQU0sY0FBYyxHQUFHLENBQUMsTUFBb0MsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLEVBQUU7SUFDcEYsSUFBSSxJQUFBLHdCQUFnQixFQUFDLE1BQU0sQ0FBQyxJQUFJLFlBQVksSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsRUFBRTtRQUNuRixPQUFPO1lBQ0wsSUFBSSxFQUFFLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDM0QsR0FBRyxZQUFZO2dCQUNmLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTTthQUNqQixDQUFDLEVBQUU7Z0JBQ0YsR0FBRyxFQUFFLEtBQUs7YUFDWCxDQUFDO1lBQ0YsU0FBUyxFQUFFLElBQUk7U0FDaEIsQ0FBQztLQUNIO0lBQ0QsT0FBTyxNQUFNLENBQUM7QUFDaEIsQ0FBQyxDQUFDO0FBRUYsTUFBTSxXQUFXLEdBQUcsQ0FBQyxNQUF1QixFQUFFLFlBQXFCLEVBQUUscUJBQXlDLEVBQUUsRUFBVSxFQUFFO0lBQzFILE1BQU0sRUFBRSxZQUFZLEVBQUUsT0FBTyxFQUFFLEdBQUcsa0JBQWtCLENBQUM7SUFDckQsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO0lBRWhCLElBQUksWUFBWSxHQUFHLFlBQVksQ0FBQztJQUVoQyxJQUFJLENBQUMsWUFBWSxFQUFFO1FBQ2pCLE1BQU0sT0FBTyxHQUFHLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUEsd0JBQWdCLEVBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQztRQUM3RixJQUFJLE9BQU8sRUFBRTtZQUNYLE1BQU0sR0FBRztnQkFDUCxHQUFHLEVBQUUsT0FBTyxDQUFDLElBQUksS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTTthQUNqRCxDQUFDO1lBQ0YsWUFBWSxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFBLHdCQUFnQixFQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNsRztLQUNGO0lBRUQsTUFBTSxNQUFNLEdBQUcsSUFBSSxpQkFBTSxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztJQUUzQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7UUFDN0IsSUFBSSxDQUFDLElBQUEsaUNBQXlCLEVBQUMsS0FBSyxDQUFDLEVBQUU7WUFDckMsTUFBTSxjQUFjLEdBQXlCLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUUxRSxJQUFJLGNBQWMsRUFBRTtnQkFDbEIsY0FBYyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLGtCQUFrQixDQUFDLENBQUM7YUFDM0Q7U0FDRjtJQUNILENBQUMsQ0FBQyxDQUFDO0lBRUgsT0FBTyxNQUFNLENBQUM7QUFDaEIsQ0FBQyxDQUFDO0FBRUYsTUFBTSxnQkFBZ0IsR0FBeUM7SUFDN0QsTUFBTSxFQUFFLENBQUMsS0FBa0IsRUFBRSxNQUFjLEVBQUUsTUFBdUIsRUFBRSxrQkFBc0MsRUFBUSxFQUFFO1FBQ3BILE1BQU0sVUFBVSxHQUFHLEVBQUUsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsa0JBQWtCLENBQUMsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUM7UUFFcEYsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNULENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLGNBQWMsQ0FBQyxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUM7U0FDckUsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELElBQUksRUFBRSxDQUFDLEtBQWdCLEVBQUUsTUFBYyxFQUFFLE1BQXVCLEVBQUUsa0JBQXNDLEVBQVEsRUFBRTtRQUNoSCxNQUFNLFVBQVUsR0FBRyxFQUFFLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLGtCQUFrQixDQUFDLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxDQUFDO1FBRXBGLE1BQU0sQ0FBQyxHQUFHLENBQUM7WUFDVCxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxjQUFjLENBQUMsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDO1NBQ3JFLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxLQUFLLEVBQUUsQ0FBQyxLQUFpQixFQUFFLE1BQWMsRUFBRSxNQUF1QixFQUFFLGtCQUFzQyxFQUFRLEVBQUU7UUFDbEgsTUFBTSxVQUFVLEdBQUcsRUFBRSxHQUFHLGdCQUFnQixDQUFDLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsQ0FBQztRQUVwRixNQUFNLENBQUMsR0FBRyxDQUFDO1lBQ1QsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsY0FBYyxDQUFDLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQztTQUNyRSxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsUUFBUSxFQUFFLENBQUMsS0FBb0IsRUFBRSxNQUFjLEVBQUUsTUFBdUIsRUFBRSxrQkFBc0MsRUFBUSxFQUFFO1FBQ3hILE1BQU0sVUFBVSxHQUFHLEVBQUUsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsa0JBQWtCLENBQUMsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUM7UUFFcEYsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNULENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLGNBQWMsQ0FBQyxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUM7U0FDckUsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELFFBQVEsRUFBRSxDQUFDLEtBQW9CLEVBQUUsTUFBYyxFQUFFLE1BQXVCLEVBQUUsa0JBQXNDLEVBQVEsRUFBRTtRQUN4SCxNQUFNLFVBQVUsR0FBRyxFQUFFLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLGtCQUFrQixDQUFDLEVBQUUsSUFBSSxFQUFFLGlCQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRWhHLE1BQU0sQ0FBQyxHQUFHLENBQUM7WUFDVCxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxjQUFjLENBQUMsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDO1NBQ3JFLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxJQUFJLEVBQUUsQ0FBQyxLQUFnQixFQUFFLE1BQWMsRUFBRSxNQUF1QixFQUFFLGtCQUFzQyxFQUFRLEVBQUU7UUFDaEgsTUFBTSxVQUFVLEdBQUcsRUFBRSxHQUFHLGdCQUFnQixDQUFDLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsQ0FBQztRQUVwRixNQUFNLENBQUMsR0FBRyxDQUFDO1lBQ1QsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsY0FBYyxDQUFDLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQztTQUNyRSxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsS0FBSyxFQUFFLENBQUMsS0FBaUIsRUFBRSxNQUFjLEVBQUUsTUFBdUIsRUFBRSxrQkFBc0MsRUFBUSxFQUFFO1FBQ2xILE1BQU0sVUFBVSxHQUErQjtZQUM3QyxJQUFJLEVBQUU7Z0JBQ0osSUFBSSxFQUFFLE1BQU07Z0JBQ1osSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDO2FBQ2hCO1lBQ0QsV0FBVyxFQUFFO2dCQUNYLElBQUksRUFBRSxDQUFDLE1BQU0sQ0FBQztnQkFDZCxRQUFRLEVBQUUsS0FBSztnQkFDZixPQUFPLEVBQUUsS0FBSyxDQUFDLFlBQVksSUFBSSxTQUFTO2FBQ3pDO1NBQ0YsQ0FBQztRQUNGLElBQUksa0JBQWtCLENBQUMsYUFBYSxJQUFJLEtBQUssQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTtZQUN2RSxVQUFVLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7U0FDdEM7UUFFRCxNQUFNLENBQUMsR0FBRyxDQUFDO1lBQ1QsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsY0FBYyxDQUFDLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQztTQUNyRSxDQUFDLENBQUM7UUFFSCxJQUFJLEtBQUssQ0FBQyxLQUFLLEtBQUssSUFBSSxJQUFJLEtBQUssQ0FBQyxLQUFLLEtBQUssU0FBUyxFQUFFO1lBQ3JELE1BQU0sWUFBWSxHQUFpQixFQUFFLENBQUM7WUFDdEMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsSUFBSSxLQUFLLENBQUMsTUFBTSxFQUFFO2dCQUNyRCxZQUFZLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDM0IsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7YUFDNUI7WUFDRCxJQUFJLEtBQUssQ0FBQyxTQUFTLElBQUksTUFBTSxDQUFDLFlBQVksRUFBRTtnQkFDMUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7b0JBQzdDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLElBQUksSUFBSSxNQUFNLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxFQUFFLFlBQVksQ0FBQyxDQUFDO2dCQUMxRSxDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxVQUFVLEVBQUUsRUFBRSxZQUFZLENBQUMsQ0FBQzthQUMxRDtTQUNGO0lBQ0gsQ0FBQztJQUNELEtBQUssRUFBRSxDQUFDLEtBQWlCLEVBQUUsTUFBYyxFQUFFLE1BQXVCLEVBQUUsa0JBQXNDLEVBQVEsRUFBRTtRQUNsSCxNQUFNLFVBQVUsR0FBRztZQUNqQixHQUFHLGdCQUFnQixDQUFDLEtBQUssRUFBRSxrQkFBa0IsQ0FBQztZQUM5QyxJQUFJLEVBQUUsTUFBTTtZQUNaLElBQUksRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO2dCQUNqQyxJQUFJLE9BQU8sTUFBTSxLQUFLLFFBQVE7b0JBQUUsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUNwRCxPQUFPLE1BQU0sQ0FBQztZQUNoQixDQUFDLENBQUM7U0FDSCxDQUFDO1FBRUYsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNULENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLGNBQWMsQ0FBQyxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUM7U0FDckUsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELFFBQVEsRUFBRSxDQUFDLEtBQW9CLEVBQUUsTUFBYyxFQUFFLE1BQXVCLEVBQUUsa0JBQXNDLEVBQVEsRUFBRTtRQUN4SCxNQUFNLFVBQVUsR0FBRyxFQUFFLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLGtCQUFrQixDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDO1FBRXJGLE1BQU0sQ0FBQyxHQUFHLENBQUM7WUFDVCxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxjQUFjLENBQUMsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDO1NBQ3JFLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxJQUFJLEVBQUUsQ0FBQyxLQUFnQixFQUFFLE1BQWMsRUFBRSxNQUF1QixFQUFFLGtCQUFzQyxFQUFRLEVBQUU7UUFDaEgsTUFBTSxVQUFVLEdBQUcsRUFBRSxHQUFHLGdCQUFnQixDQUFDLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQztRQUVsRixNQUFNLENBQUMsR0FBRyxDQUFDO1lBQ1QsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsY0FBYyxDQUFDLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQztTQUNyRSxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsTUFBTSxFQUFFLENBQUMsS0FBa0IsRUFBRSxNQUFjLEVBQUUsTUFBdUIsRUFBRSxrQkFBc0MsRUFBUSxFQUFFO1FBQ3BILE1BQU0sVUFBVSxHQUFHO1lBQ2pCLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLGtCQUFrQixDQUFDO1lBQzlDLElBQUksRUFBRSxpQkFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLO1lBQ3hCLEdBQUcsRUFBRSxLQUFLLENBQUMsVUFBVTtTQUN0QixDQUFDO1FBRUYsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNULENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLGNBQWMsQ0FBQyxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUM7U0FDckUsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELFlBQVksRUFBRSxDQUFDLEtBQXdCLEVBQUUsTUFBYyxFQUFFLE1BQXVCLEVBQUUsa0JBQXNDLEVBQUUsRUFBRTtRQUMxSCxNQUFNLGdCQUFnQixHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3pELElBQUksY0FBYyxHQUEyQixFQUFFLENBQUM7UUFFaEQsSUFBSSxLQUFLLENBQUMsU0FBUyxJQUFJLE1BQU0sQ0FBQyxZQUFZLEVBQUU7WUFDMUMsY0FBYyxHQUFHO2dCQUNmLElBQUksRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7b0JBQzNELElBQUksWUFBWSxHQUEyQixFQUFFLENBQUM7b0JBRTlDLElBQUksZ0JBQWdCLEVBQUU7d0JBQ3BCLFlBQVksQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDO3dCQUN6QixZQUFZLENBQUMsS0FBSyxHQUFHOzRCQUNuQixJQUFJLEVBQUUsaUJBQU0sQ0FBQyxLQUFLLENBQUMsS0FBSzs0QkFDeEIsT0FBTyxFQUFFLEdBQUcsS0FBSyxDQUFDLElBQUksSUFBSSxNQUFNLGFBQWE7eUJBQzlDLENBQUM7d0JBQ0YsWUFBWSxDQUFDLFVBQVUsR0FBRyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxVQUFVLEVBQUUsQ0FBQztxQkFDcEU7eUJBQU07d0JBQ0wsWUFBWSxHQUFHOzRCQUNiLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLGtCQUFrQixDQUFDOzRCQUM5QyxJQUFJLEVBQUUsaUJBQU0sQ0FBQyxLQUFLLENBQUMsS0FBSzs0QkFDeEIsR0FBRyxFQUFFLEtBQUssQ0FBQyxVQUFVO3lCQUN0QixDQUFDO3FCQUNIO29CQUVELE9BQU87d0JBQ0wsR0FBRyxPQUFPO3dCQUNWLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWTtxQkFDeEQsQ0FBQztnQkFDSixDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUNOLFNBQVMsRUFBRSxJQUFJO2FBQ2hCLENBQUM7U0FDSDthQUFNLElBQUksZ0JBQWdCLEVBQUU7WUFDM0IsY0FBYyxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUM7WUFDM0IsY0FBYyxDQUFDLEtBQUssR0FBRztnQkFDckIsSUFBSSxFQUFFLGlCQUFNLENBQUMsS0FBSyxDQUFDLEtBQUs7Z0JBQ3hCLE9BQU8sRUFBRSxHQUFHLEtBQUssQ0FBQyxJQUFJLGFBQWE7YUFDcEMsQ0FBQztZQUNGLGNBQWMsQ0FBQyxVQUFVLEdBQUcsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7WUFFckUsSUFBSSxLQUFLLENBQUMsT0FBTztnQkFBRSxjQUFjLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUN0RDthQUFNO1lBQ0wsY0FBYyxHQUFHO2dCQUNmLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLGtCQUFrQixDQUFDO2dCQUM5QyxJQUFJLEVBQUUsaUJBQU0sQ0FBQyxLQUFLLENBQUMsS0FBSztnQkFDeEIsR0FBRyxFQUFFLEtBQUssQ0FBQyxVQUFVO2FBQ3RCLENBQUM7WUFFRixJQUFJLEtBQUssQ0FBQyxPQUFPO2dCQUFFLGNBQWMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ3REO1FBRUQsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNULENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLGNBQWM7U0FDN0IsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELEdBQUcsRUFBRSxDQUFDLEtBQWUsRUFBRSxNQUFjLEVBQUUsTUFBdUIsRUFBRSxrQkFBc0MsRUFBUSxFQUFFO1FBQzlHLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBZSxFQUFFLEVBQUU7WUFDdkMsTUFBTSxjQUFjLEdBQXlCLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUU3RSxJQUFJLGNBQWMsRUFBRTtnQkFDbEIsY0FBYyxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLGtCQUFrQixDQUFDLENBQUM7YUFDOUQ7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxXQUFXLEVBQUUsQ0FBQyxLQUF1QixFQUFFLE1BQWMsRUFBRSxNQUF1QixFQUFFLGtCQUFzQyxFQUFRLEVBQUU7UUFDOUgsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFlLEVBQUUsRUFBRTtZQUN2QyxNQUFNLGNBQWMsR0FBeUIsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRTdFLElBQUksY0FBYyxFQUFFO2dCQUNsQixjQUFjLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsa0JBQWtCLENBQUMsQ0FBQzthQUM5RDtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELElBQUksRUFBRSxDQUFDLEtBQWdCLEVBQUUsTUFBYyxFQUFFLE1BQXVCLEVBQUUsa0JBQXNDLEVBQVEsRUFBRTtRQUNoSCxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQ3pCLElBQUksSUFBQSxrQkFBVSxFQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNuQixNQUFNLFVBQVUsR0FBRztvQkFDakIsSUFBSSxFQUFFLFdBQVcsQ0FDZixNQUFNLEVBQ04sR0FBRyxDQUFDLE1BQU0sRUFDVjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1AsR0FBRyxFQUFFLEtBQUs7NEJBQ1YsRUFBRSxFQUFFLEtBQUs7eUJBQ1Y7d0JBQ0QsYUFBYSxFQUFFLGtCQUFrQixDQUFDLGFBQWE7cUJBQ2hELENBQ0Y7aUJBQ0YsQ0FBQztnQkFFRixNQUFNLENBQUMsR0FBRyxDQUFDO29CQUNULENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLGNBQWMsQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUM7aUJBQ2pFLENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNKLEdBQWtCLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQWUsRUFBRSxFQUFFO29CQUNyRCxNQUFNLGNBQWMsR0FBeUIsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUU3RSxJQUFJLGNBQWMsRUFBRTt3QkFDbEIsY0FBYyxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLGtCQUFrQixDQUFDLENBQUM7cUJBQzlEO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxLQUFLLEVBQUUsQ0FBQyxLQUFpQixFQUFFLE1BQWMsRUFBRSxNQUF1QixFQUFFLGtCQUFzQyxFQUFFLEVBQUU7UUFDNUcsTUFBTSxVQUFVLEdBQUc7WUFDakIsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsa0JBQWtCLENBQUM7WUFDOUMsSUFBSSxFQUFFLENBQUMsV0FBVyxDQUNoQixNQUFNLEVBQ04sS0FBSyxDQUFDLE1BQU0sRUFDWjtvQkFDRSxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUU7b0JBQ2xDLFlBQVksRUFBRSxJQUFJO29CQUNsQixhQUFhLEVBQUUsa0JBQWtCLENBQUMsYUFBYTtpQkFDaEQsQ0FDRixDQUFDO1NBQ0gsQ0FBQztRQUVGLE1BQU0sQ0FBQyxHQUFHLENBQUM7WUFDVCxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxjQUFjLENBQUMsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDO1NBQ3JFLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxLQUFLLEVBQUUsQ0FBQyxLQUFpQixFQUFFLE1BQWMsRUFBRSxNQUF1QixFQUFFLGtCQUFzQyxFQUFRLEVBQUU7UUFDbEgsTUFBTSxtQkFBbUIsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUV4RSxNQUFNLFVBQVUsR0FBRztZQUNqQixHQUFHLG1CQUFtQjtZQUN0QixJQUFJLEVBQUUsV0FBVyxDQUNmLE1BQU0sRUFDTixLQUFLLENBQUMsTUFBTSxFQUNaO2dCQUNFLE9BQU8sRUFBRTtvQkFDUCxHQUFHLEVBQUUsS0FBSztvQkFDVixFQUFFLEVBQUUsS0FBSztpQkFDVjtnQkFDRCxhQUFhLEVBQUUsa0JBQWtCLENBQUMsYUFBYTthQUNoRCxDQUNGO1NBQ0YsQ0FBQztRQUVGLE1BQU0sQ0FBQyxHQUFHLENBQUM7WUFDVCxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxjQUFjLENBQUMsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDO1NBQ3JFLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxNQUFNLEVBQUUsQ0FBQyxLQUFrQixFQUFFLE1BQWMsRUFBRSxNQUF1QixFQUFFLGtCQUFzQyxFQUFRLEVBQUU7UUFDcEgsTUFBTSxVQUFVLEdBQUc7WUFDakIsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsa0JBQWtCLENBQUM7WUFDOUMsSUFBSSxFQUFFLE1BQU07WUFDWixJQUFJLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTtnQkFDakMsSUFBSSxPQUFPLE1BQU0sS0FBSyxRQUFRO29CQUFFLE9BQU8sTUFBTSxDQUFDLEtBQUssQ0FBQztnQkFDcEQsT0FBTyxNQUFNLENBQUM7WUFDaEIsQ0FBQyxDQUFDO1NBQ0gsQ0FBQztRQUNGLE1BQU0sY0FBYyxHQUFHLGNBQWMsQ0FBQyxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUU5RSxNQUFNLENBQUMsR0FBRyxDQUFDO1lBQ1QsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYztTQUNoRSxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsTUFBTSxFQUFFLENBQUMsS0FBaUIsRUFBRSxNQUFjLEVBQUUsTUFBdUIsRUFBRSxrQkFBc0MsRUFBUSxFQUFFO1FBQ25ILE1BQU0sV0FBVyxHQUFHLENBQUMsSUFBSSxpQkFBTSxDQUFDLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsZ0JBQWdCLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBRXBGLE1BQU0sQ0FBQyxHQUFHLENBQUM7WUFDVCxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxjQUFjLENBQUMsS0FBSyxFQUFFLFdBQVcsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDO1NBQ3RFLENBQUMsQ0FBQztRQUVILEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBZ0IsRUFBRSxFQUFFO1lBQ3hDLE1BQU0sV0FBVyxHQUFHLElBQUksaUJBQU0sQ0FBQyxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBRTlELFNBQVMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsVUFBVSxFQUFFLEVBQUU7Z0JBQ3RDLE1BQU0sY0FBYyxHQUF5QixnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQy9FLElBQUksY0FBYyxFQUFFO29CQUNsQixjQUFjLENBQUMsVUFBVSxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztpQkFDckU7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUVILElBQUksS0FBSyxDQUFDLFNBQVMsSUFBSSxNQUFNLENBQUMsWUFBWSxFQUFFO2dCQUMxQyxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTtvQkFDN0MsNkRBQTZEO29CQUM3RCxxRUFBcUU7b0JBQ3JFLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsSUFBSSxJQUFJLE1BQU0sRUFBRSxDQUFDLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQ3BGLENBQUMsQ0FBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsNkRBQTZEO2dCQUM3RCxxRUFBcUU7Z0JBQ3JFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO2FBQ3BFO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0YsQ0FBQztBQUVGLGtCQUFlLFdBQVcsQ0FBQyJ9