"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const file_type_1 = require("file-type");
const fs_1 = __importDefault(require("fs"));
const sanitize_filename_1 = __importDefault(require("sanitize-filename"));
const sharp_1 = __importDefault(require("sharp"));
const fileExists_1 = __importDefault(require("./fileExists"));
function getOutputImage(sourceImage, size) {
    const extension = sourceImage.split('.').pop();
    const name = (0, sanitize_filename_1.default)(sourceImage.substring(0, sourceImage.lastIndexOf('.')) || sourceImage);
    return {
        name,
        extension,
        width: size.width,
        height: size.height,
    };
}
async function resizeAndSave({ req, file, dimensions, staticPath, config, savedFilename, }) {
    const { imageSizes, disableLocalStorage } = config.upload;
    const sizes = imageSizes
        .filter((desiredSize) => needsResize(desiredSize, dimensions))
        .map(async (desiredSize) => {
        let resized = (0, sharp_1.default)(file).resize(desiredSize);
        if (desiredSize.formatOptions) {
            resized = resized.toFormat(desiredSize.formatOptions.format, desiredSize.formatOptions.options);
        }
        const bufferObject = await resized.toBuffer({
            resolveWithObject: true,
        });
        req.payloadUploadSizes[desiredSize.name] = bufferObject.data;
        const outputImage = getOutputImage(savedFilename, desiredSize);
        const imageNameWithDimensions = createImageName(outputImage, bufferObject);
        const imagePath = `${staticPath}/${imageNameWithDimensions}`;
        const fileAlreadyExists = await (0, fileExists_1.default)(imagePath);
        if (fileAlreadyExists) {
            fs_1.default.unlinkSync(imagePath);
        }
        if (!disableLocalStorage) {
            await resized.toFile(imagePath);
        }
        return {
            name: desiredSize.name,
            width: bufferObject.info.width,
            height: bufferObject.info.height,
            filename: imageNameWithDimensions,
            filesize: bufferObject.info.size,
            mimeType: (await (0, file_type_1.fromBuffer)(bufferObject.data)).mime,
        };
    });
    const savedSizes = await Promise.all(sizes);
    return savedSizes.reduce((results, size) => ({
        ...results,
        [size.name]: {
            width: size.width,
            height: size.height,
            filename: size.filename,
            mimeType: size.mimeType,
            filesize: size.filesize,
        },
    }), {});
}
exports.default = resizeAndSave;
function createImageName(outputImage, bufferObject) {
    return `${outputImage.name}-${bufferObject.info.width}x${bufferObject.info.height}.${outputImage.extension}`;
}
function needsResize(desiredSize, dimensions) {
    return (typeof desiredSize.width === 'number' && desiredSize.width <= dimensions.width)
        || (typeof desiredSize.height === 'number' && desiredSize.height <= dimensions.height);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1hZ2VSZXNpemVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL3VwbG9hZHMvaW1hZ2VSZXNpemVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEseUNBQXVDO0FBQ3ZDLDRDQUFvQjtBQUNwQiwwRUFBeUM7QUFDekMsa0RBQTBCO0FBRzFCLDhEQUFzQztBQXFCdEMsU0FBUyxjQUFjLENBQUMsV0FBbUIsRUFBRSxJQUFlO0lBQzFELE1BQU0sU0FBUyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDL0MsTUFBTSxJQUFJLEdBQUcsSUFBQSwyQkFBUSxFQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxXQUFXLENBQUMsQ0FBQztJQUU3RixPQUFPO1FBQ0wsSUFBSTtRQUNKLFNBQVM7UUFDVCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7UUFDakIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO0tBQ3BCLENBQUM7QUFDSixDQUFDO0FBRWMsS0FBSyxVQUFVLGFBQWEsQ0FBQyxFQUMxQyxHQUFHLEVBQ0gsSUFBSSxFQUNKLFVBQVUsRUFDVixVQUFVLEVBQ1YsTUFBTSxFQUNOLGFBQWEsR0FDUjtJQUNMLE1BQU0sRUFBRSxVQUFVLEVBQUUsbUJBQW1CLEVBQUUsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBRTFELE1BQU0sS0FBSyxHQUFHLFVBQVU7U0FDckIsTUFBTSxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1NBQzdELEdBQUcsQ0FBQyxLQUFLLEVBQUUsV0FBVyxFQUFFLEVBQUU7UUFDekIsSUFBSSxPQUFPLEdBQUcsSUFBQSxlQUFLLEVBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRTlDLElBQUksV0FBVyxDQUFDLGFBQWEsRUFBRTtZQUM3QixPQUFPLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxXQUFXLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ2pHO1FBRUQsTUFBTSxZQUFZLEdBQUcsTUFBTSxPQUFPLENBQUMsUUFBUSxDQUFDO1lBQzFDLGlCQUFpQixFQUFFLElBQUk7U0FDeEIsQ0FBQyxDQUFDO1FBRUgsR0FBRyxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDO1FBRTdELE1BQU0sV0FBVyxHQUFHLGNBQWMsQ0FBQyxhQUFhLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFDL0QsTUFBTSx1QkFBdUIsR0FBRyxlQUFlLENBQUMsV0FBVyxFQUFFLFlBQVksQ0FBQyxDQUFDO1FBQzNFLE1BQU0sU0FBUyxHQUFHLEdBQUcsVUFBVSxJQUFJLHVCQUF1QixFQUFFLENBQUM7UUFDN0QsTUFBTSxpQkFBaUIsR0FBRyxNQUFNLElBQUEsb0JBQVUsRUFBQyxTQUFTLENBQUMsQ0FBQztRQUV0RCxJQUFJLGlCQUFpQixFQUFFO1lBQ3JCLFlBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDMUI7UUFFRCxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDeEIsTUFBTSxPQUFPLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ2pDO1FBRUQsT0FBTztZQUNMLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSTtZQUN0QixLQUFLLEVBQUUsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLO1lBQzlCLE1BQU0sRUFBRSxZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU07WUFDaEMsUUFBUSxFQUFFLHVCQUF1QjtZQUNqQyxRQUFRLEVBQUUsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJO1lBQ2hDLFFBQVEsRUFBRSxDQUFDLE1BQU0sSUFBQSxzQkFBVSxFQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUk7U0FDckQsQ0FBQztJQUNKLENBQUMsQ0FBQyxDQUFDO0lBRUwsTUFBTSxVQUFVLEdBQUcsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBRTVDLE9BQU8sVUFBVSxDQUFDLE1BQU0sQ0FDdEIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ2xCLEdBQUcsT0FBTztRQUNWLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ1gsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtZQUNuQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtTQUN4QjtLQUNGLENBQUMsRUFDRixFQUFFLENBQ0gsQ0FBQztBQUNKLENBQUM7QUEvREQsZ0NBK0RDO0FBQ0QsU0FBUyxlQUFlLENBQ3RCLFdBQXdCLEVBQ3hCLFlBQXNEO0lBRXRELE9BQU8sR0FBRyxXQUFXLENBQUMsSUFBSSxJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLFdBQVcsQ0FBQyxTQUFTLEVBQUUsQ0FBQztBQUMvRyxDQUFDO0FBRUQsU0FBUyxXQUFXLENBQUMsV0FBc0IsRUFBRSxVQUEyQjtJQUN0RSxPQUFPLENBQUMsT0FBTyxXQUFXLENBQUMsS0FBSyxLQUFLLFFBQVEsSUFBSSxXQUFXLENBQUMsS0FBSyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUM7V0FDbEYsQ0FBQyxPQUFPLFdBQVcsQ0FBQyxNQUFNLEtBQUssUUFBUSxJQUFJLFdBQVcsQ0FBQyxNQUFNLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQzNGLENBQUMifQ==