"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const file_type_1 = require("file-type");
const mkdirp_1 = __importDefault(require("mkdirp"));
const path_1 = __importDefault(require("path"));
const sanitize_filename_1 = __importDefault(require("sanitize-filename"));
const sharp_1 = __importDefault(require("sharp"));
const errors_1 = require("../errors");
const getImageSize_1 = __importDefault(require("./getImageSize"));
const getSafeFilename_1 = __importDefault(require("./getSafeFilename"));
const imageResizer_1 = __importDefault(require("./imageResizer"));
const saveBufferToFile_1 = __importDefault(require("./saveBufferToFile"));
const canResizeImage_1 = __importDefault(require("./canResizeImage"));
const uploadFile = async ({ config, collection: { config: collectionConfig, Model, }, req, data, throwOnMissingFile, overwriteExistingFiles, }) => {
    var _a;
    let newData = data;
    if (collectionConfig.upload) {
        const fileData = {};
        const { staticDir, imageSizes, disableLocalStorage, resizeOptions, formatOptions } = collectionConfig.upload;
        const { file } = req.files || {};
        if (throwOnMissingFile && !file) {
            throw new errors_1.MissingFile();
        }
        let staticPath = staticDir;
        if (staticDir.indexOf('/') !== 0) {
            staticPath = path_1.default.resolve(config.paths.configDir, staticDir);
        }
        if (!disableLocalStorage) {
            mkdirp_1.default.sync(staticPath);
        }
        if (file) {
            try {
                const shouldResize = (0, canResizeImage_1.default)(file.mimetype);
                let fsSafeName;
                let resized;
                let dimensions;
                if (shouldResize) {
                    if (resizeOptions) {
                        resized = (0, sharp_1.default)(file.data)
                            .resize(resizeOptions);
                    }
                    if (formatOptions) {
                        resized = (resized !== null && resized !== void 0 ? resized : (0, sharp_1.default)(file.data)).toFormat(formatOptions.format, formatOptions.options);
                    }
                    dimensions = await (0, getImageSize_1.default)(file);
                    fileData.width = dimensions.width;
                    fileData.height = dimensions.height;
                }
                const fileBuffer = resized ? (await resized.toBuffer()) : file.data;
                const { mime, ext } = (_a = await (0, file_type_1.fromBuffer)(fileBuffer)) !== null && _a !== void 0 ? _a : { mime: file.mimetype, ext: file.name.split('.').pop() };
                const fileSize = fileBuffer.length;
                const baseFilename = (0, sanitize_filename_1.default)(file.name.substring(0, file.name.lastIndexOf('.')) || file.name);
                fsSafeName = `${baseFilename}.${ext}`;
                if (!overwriteExistingFiles) {
                    fsSafeName = await (0, getSafeFilename_1.default)(Model, staticPath, fsSafeName);
                }
                if (!disableLocalStorage) {
                    await (0, saveBufferToFile_1.default)(fileBuffer, `${staticPath}/${fsSafeName}`);
                }
                fileData.filename = fsSafeName || (!overwriteExistingFiles ? await (0, getSafeFilename_1.default)(Model, staticPath, file.name) : file.name);
                fileData.filesize = fileSize || file.size;
                fileData.mimeType = mime || (await (0, file_type_1.fromBuffer)(file.data)).mime;
                if (Array.isArray(imageSizes) && shouldResize) {
                    req.payloadUploadSizes = {};
                    fileData.sizes = await (0, imageResizer_1.default)({
                        req,
                        file: file.data,
                        dimensions,
                        staticPath,
                        config: collectionConfig,
                        savedFilename: fsSafeName || file.name,
                        mimeType: fileData.mimeType,
                    });
                }
            }
            catch (err) {
                console.error(err);
                throw new errors_1.FileUploadError();
            }
            newData = {
                ...newData,
                ...fileData,
            };
        }
    }
    return newData;
};
exports.default = uploadFile;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkRmlsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91cGxvYWRzL3VwbG9hZEZpbGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSx5Q0FBdUM7QUFDdkMsb0RBQTRCO0FBQzVCLGdEQUF3QjtBQUN4QiwwRUFBeUM7QUFDekMsa0RBQXFDO0FBR3JDLHNDQUF5RDtBQUV6RCxrRUFBK0Q7QUFDL0Qsd0VBQWdEO0FBQ2hELGtFQUEyQztBQUMzQywwRUFBa0Q7QUFFbEQsc0VBQThDO0FBVzlDLE1BQU0sVUFBVSxHQUFHLEtBQUssRUFBRSxFQUN4QixNQUFNLEVBQ04sVUFBVSxFQUFFLEVBQ1YsTUFBTSxFQUFFLGdCQUFnQixFQUN4QixLQUFLLEdBQ04sRUFDRCxHQUFHLEVBQ0gsSUFBSSxFQUNKLGtCQUFrQixFQUNsQixzQkFBc0IsR0FDakIsRUFBb0MsRUFBRTs7SUFDM0MsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDO0lBRW5CLElBQUksZ0JBQWdCLENBQUMsTUFBTSxFQUFFO1FBQzNCLE1BQU0sUUFBUSxHQUFzQixFQUFFLENBQUM7UUFFdkMsTUFBTSxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsbUJBQW1CLEVBQUUsYUFBYSxFQUFFLGFBQWEsRUFBRSxHQUFHLGdCQUFnQixDQUFDLE1BQU0sQ0FBQztRQUU3RyxNQUFNLEVBQUUsSUFBSSxFQUFFLEdBQUcsR0FBRyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7UUFFakMsSUFBSSxrQkFBa0IsSUFBSSxDQUFDLElBQUksRUFBRTtZQUMvQixNQUFNLElBQUksb0JBQVcsRUFBRSxDQUFDO1NBQ3pCO1FBRUQsSUFBSSxVQUFVLEdBQUcsU0FBUyxDQUFDO1FBRTNCLElBQUksU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDaEMsVUFBVSxHQUFHLGNBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7U0FDOUQ7UUFFRCxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDeEIsZ0JBQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDekI7UUFFRCxJQUFJLElBQUksRUFBRTtZQUNSLElBQUk7Z0JBQ0YsTUFBTSxZQUFZLEdBQUcsSUFBQSx3QkFBYyxFQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDbkQsSUFBSSxVQUFrQixDQUFDO2dCQUN2QixJQUFJLE9BQTBCLENBQUM7Z0JBQy9CLElBQUksVUFBMkIsQ0FBQztnQkFDaEMsSUFBSSxZQUFZLEVBQUU7b0JBQ2hCLElBQUksYUFBYSxFQUFFO3dCQUNqQixPQUFPLEdBQUcsSUFBQSxlQUFLLEVBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzs2QkFDdkIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO3FCQUMxQjtvQkFDRCxJQUFJLGFBQWEsRUFBRTt3QkFDakIsT0FBTyxHQUFHLENBQUMsT0FBTyxhQUFQLE9BQU8sY0FBUCxPQUFPLEdBQUksSUFBQSxlQUFLLEVBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUMvRjtvQkFDRCxVQUFVLEdBQUcsTUFBTSxJQUFBLHNCQUFZLEVBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQztvQkFDbEMsUUFBUSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDO2lCQUNyQztnQkFFRCxNQUFNLFVBQVUsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFFcEUsTUFBTSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsR0FBRyxNQUFBLE1BQU0sSUFBQSxzQkFBVSxFQUFDLFVBQVUsQ0FBQyxtQ0FBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDO2dCQUMvRyxNQUFNLFFBQVEsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDO2dCQUNuQyxNQUFNLFlBQVksR0FBRyxJQUFBLDJCQUFRLEVBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMvRixVQUFVLEdBQUcsR0FBRyxZQUFZLElBQUksR0FBRyxFQUFFLENBQUM7Z0JBRXRDLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtvQkFDM0IsVUFBVSxHQUFHLE1BQU0sSUFBQSx5QkFBZSxFQUFDLEtBQUssRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7aUJBQ25FO2dCQUVELElBQUksQ0FBQyxtQkFBbUIsRUFBRTtvQkFDeEIsTUFBTSxJQUFBLDBCQUFnQixFQUFDLFVBQVUsRUFBRSxHQUFHLFVBQVUsSUFBSSxVQUFVLEVBQUUsQ0FBQyxDQUFDO2lCQUNuRTtnQkFFRCxRQUFRLENBQUMsUUFBUSxHQUFHLFVBQVUsSUFBSSxDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBQSx5QkFBZSxFQUFDLEtBQUssRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzlILFFBQVEsQ0FBQyxRQUFRLEdBQUcsUUFBUSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQzFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFBLHNCQUFVLEVBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUUvRCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksWUFBWSxFQUFFO29CQUM3QyxHQUFHLENBQUMsa0JBQWtCLEdBQUcsRUFBRSxDQUFDO29CQUM1QixRQUFRLENBQUMsS0FBSyxHQUFHLE1BQU0sSUFBQSxzQkFBYSxFQUFDO3dCQUNuQyxHQUFHO3dCQUNILElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTt3QkFDZixVQUFVO3dCQUNWLFVBQVU7d0JBQ1YsTUFBTSxFQUFFLGdCQUFnQjt3QkFDeEIsYUFBYSxFQUFFLFVBQVUsSUFBSSxJQUFJLENBQUMsSUFBSTt3QkFDdEMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxRQUFRO3FCQUM1QixDQUFDLENBQUM7aUJBQ0o7YUFDRjtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNaLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ25CLE1BQU0sSUFBSSx3QkFBZSxFQUFFLENBQUM7YUFDN0I7WUFFRCxPQUFPLEdBQUc7Z0JBQ1IsR0FBRyxPQUFPO2dCQUNWLEdBQUcsUUFBUTthQUNaLENBQUM7U0FDSDtLQUNGO0lBRUQsT0FBTyxPQUFPLENBQUM7QUFDakIsQ0FBQyxDQUFDO0FBRUYsa0JBQWUsVUFBVSxDQUFDIn0=