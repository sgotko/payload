import { AdminUILocale } from "payload/config";

const en: AdminUILocale = {
  ArrayAction: {
    MoveUpLabel: "Move Up",
    MoveDownLabel: "Move Down",
    AddBelowLabel: "Add Below",
    DublicateLabel: "Dublicate",
    RemoveLabel: "Remove",
  },
};

export { en };

export default en;
